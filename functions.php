<?php
/*
Author: Eddie Machado
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images, 
sidebars, comments, ect.
*/

// Get Bones Core Up & Running!
require_once('library/bones.php');            // core functions (don't remove)
require_once('library/plugins.php');          // plugins & extra functions (optional)

// Options panel
require_once('library/options-panel.php');

// Shortcodes
require_once('library/shortcodes.php');

// Admin Functions (commented out by default)
// require_once('library/admin.php');         // custom admin functions

// Custom Backend Footer
function bones_custom_admin_footer() {
	echo '<span id="footer-thankyou">Developed by <a href="http://onlinevortex.com" target="_blank">Onlinevortex</a></span>.';
}

// adding it to the admin area
add_filter('admin_footer_text', 'bones_custom_admin_footer');

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'wpf-featured', 639, 300, true );
add_image_size ( 'wpf-home-featured', 970, 364, true );
add_image_size( 'bones-thumb-300', 300, 180, true );
add_image_size( 'bones-thumb-130', 130, 80, true );
add_image_size( 'bones-thumb-161', 161, 129, true );
/* 
to add more sizes, simply copy a line from above 
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image, 
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
    register_sidebar(array(
    	'id' => 'sidebar1',
    	'name' => 'Main Sidebar',
    	'description' => 'Used on every page BUT the homepage page template.',
    	'before_widget' => '<div id="%1$s" class="widget %2$s">',
    	'after_widget' => '</div>',
    	'before_title' => '<h4 class="widgettitle">',
    	'after_title' => '</h4>',
    ));
    
    register_sidebar(array(
    	'id' => 'sidebar2',
    	'name' => 'Homepage Sidebar',
    	'description' => 'Used only on the homepage page template.',
    	'before_widget' => '<div id="%1$s" class="widget %2$s">',
    	'after_widget' => '</div>',
    	'before_title' => '<h4 class="widgettitle">',
    	'after_title' => '</h4>',
    ));
    
    /* 
    to add more sidebars or widgetized areas, just copy
    and edit the above sidebar code. In order to call 
    your new sidebar just use the following code:
    
    Just change the name to whatever your new
    sidebar's id is, for example:
    
    
    
    To call the sidebar in your template, you can just copy
    the sidebar.php file and rename it to your sidebar's name.
    So using the above example, it would be:
    sidebar-sidebar2.php
    
    */
} // don't remove this bracket!

/************* ENQUEUE CSS AND JS *****************/

function theme_styles()  
{ 
    // Bring in Open Sans from Google fonts
    wp_register_style( 'open-sans', 'http://fonts.googleapis.com/css?family=Open+Sans:300,800');
    // This is the compiled css file from SCSS
    wp_register_style( 'foundation-app', get_template_directory_uri() . '/stylesheets/app.css', array(), '3.0', 'all' );
    
    wp_enqueue_style( 'open-sans' );
    wp_enqueue_style( 'foundation-app' );
}

add_action('wp_enqueue_scripts', 'theme_styles');

/************* ENQUEUE JS *************************/

function load_local_jQuery() {  
    wp_deregister_script('jquery'); // initiate the function  
    wp_register_script('jquery', get_template_directory_uri() .'/javascripts/jquery.min.js', __FILE__, false, '1.7.2', true); // register the local file  
    wp_enqueue_script('jquery'); // enqueue the local file  
}  

add_action('wp_enqueue_scripts', 'load_local_jQuery', 1); // initiate the function  

/* load modernizr from foundation */
function modernize_it(){
    wp_register_script( 'modernizr', get_template_directory_uri() . '/javascripts/foundation/modernizr.foundation.js' ); 
    wp_enqueue_script( 'modernizr' );
}

add_action( 'wp_enqueue_scripts', 'modernize_it' );

function foundation_js(){
    wp_register_script( 'foundation-reveal', get_template_directory_uri() . '/javascripts/foundation/jquery.reveal.js', array('jquery'), '1.1', true ); 
    wp_enqueue_script( 'foundation-reveal' );
    wp_register_script( 'foundation-orbit', get_template_directory_uri() . '/javascripts/foundation/jquery.orbit-1.4.0.js', array('jquery'), '1.4.0', true ); 
    wp_enqueue_script( 'foundation-orbit' );
    wp_register_script( 'foundation-custom-forms', get_template_directory_uri() . '/javascripts/foundation/jquery.customforms.js', array('jquery'), '1.0', true ); 
    wp_enqueue_script( 'foundation-custom-forms' );
    wp_register_script( 'foundation-placeholder', get_template_directory_uri() . '/javascripts/foundation/jquery.placeholder.min.js', array('jquery'), '2.0.7', true ); 
    wp_enqueue_script( 'foundation-placeholder' );
    wp_register_script( 'foundation-tooltips', get_template_directory_uri() . '/javascripts/foundation/jquery.tooltips.js', array('jquery'), '2.0.1', true ); 
    wp_enqueue_script( 'foundation-tooltips' );
    wp_register_script( 'foundation-app', get_template_directory_uri() . '/javascripts/app.js', array('jquery'), '1.0', true ); 
    wp_enqueue_script( 'foundation-app' );
    wp_register_script( 'foundation-off-canvas', get_template_directory_uri() . '/javascripts/foundation/off-canvas.js', array('jquery'), '1.0', true ); 
    wp_enqueue_script( 'foundation-off-canvas');
}

add_action('wp_enqueue_scripts', 'foundation_js', 99);

function wp_foundation_js(){
    wp_register_script( 'wp-foundation-js', get_template_directory_uri() . '/library/js/scripts.js', 'jQuery', '1.0', true);
    wp_enqueue_script( 'wp-foundation-js' );
}

add_action('wp_enqueue_scripts', 'wp_foundation_js');

/************* COMMENT LAYOUT *********************/
		
// Comment Layout
function bones_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="panel clearfix">
			<div class="comment-author vcard row clearfix">
                <div class="twelve columns">
                    <div class="
                        <?php
                        $authID = get_the_author_meta('ID');
                                                    
                        if($authID == $comment->user_id)
                            echo "panel callout";
                        else
                            echo "panel";
                        ?>
                    ">
                        <div class="row">
            				<div class="avatar two columns">
            					<?php echo get_avatar($comment,$size='75',$default='<path_to_url>' ); ?>
            				</div>
            				<div class="ten columns">
            					<?php printf(__('<h4 class="span8">%s</h4>'), get_comment_author_link()) ?>
            					<time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time('F jS, Y'); ?> </a></time>
            					
            					<?php edit_comment_link(__('Edit'),'<span class="edit-comment">', '</span>'); ?>
                                
                                <?php if ($comment->comment_approved == '0') : ?>
                   					<div class="alert-box success">
                      					<?php _e('Your comment is awaiting moderation.') ?>
                      				</div>
            					<?php endif; ?>
                                
                                <?php comment_text() ?>
                                
                                <!-- removing reply link on each comment since we're not nesting them -->
            					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</article>
    <!-- </li> is added by wordpress automatically -->
<?php
} // don't remove this bracket!

// Add grid classes to comments
function add_class_comments($classes){
    array_push($classes, "twelve", "columns");
    return $classes;
}
add_filter('comment_class', 'add_class_comments');

/************* SEARCH FORM LAYOUT *****************/

// Search Form
function bones_wpsearch($form) {
    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <label class="screen-reader-text" for="s">' . __('Search for:', 'bonestheme') . '</label>
    <input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="Search the Site..." />
    <input type="submit" id="searchsubmit" value="'. esc_attr__('Search') .'" />
    </form>';
    return $form;
} // don't remove this bracket!

/****************** password protected post form *****/

add_filter( 'the_password_form', 'custom_password_form' );

function custom_password_form() {
	global $post;
	$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
	$o = '<div class="clearfix"><form action="' . get_option('siteurl') . '/wp-pass.php" method="post">
	' . __( "<p>This post is password protected. To view it please enter your password below:</p>" ) . '
	<div class="row collapse">
        <div class="twelve columns"><label for="' . $label . '">' . __( "Password:" ) . ' </label></div>
        <div class="eight columns">
            <input name="post_password" id="' . $label . '" type="password" size="20" class="input-text" />
        </div>
        <div class="four columns">
            <input type="submit" name="Submit" class="postfix button nice blue radius" value="' . esc_attr__( "Submit" ) . '" />
        </div>
	</div>
    </form></div>
	';
	return $o;
}

/*********** update standard wp tag cloud widget so it looks better ************/

// filter tag clould output so that it can be styled by CSS
function add_tag_class( $taglinks ) {
    $tags = explode('</a>', $taglinks);
    $regex = "#(.*tag-link[-])(.*)(' title.*)#e";
        foreach( $tags as $tag ) {
            $tagn[] = preg_replace($regex, "('$1$2 label radius tag-'.get_tag($2)->slug.'$3')", $tag );
        }
    $taglinks = implode('</a>', $tagn);
    return $taglinks;
}

add_action('wp_tag_cloud', 'add_tag_class');

add_filter( 'widget_tag_cloud_args', 'my_widget_tag_cloud_args' );

function my_widget_tag_cloud_args( $args ) {
	$args['number'] = 20; // show less tags
	$args['largest'] = 9.75; // make largest and smallest the same - i don't like the varying font-size look
	$args['smallest'] = 9.75;
	$args['unit'] = 'px';
	return $args;
}

add_filter('wp_tag_cloud','wp_tag_cloud_filter', 10, 2);

function wp_tag_cloud_filter($return, $args)
{
  return '<div id="tag-cloud"><p>'.$return.'</p></div>';
}

function add_class_the_tags($html){
    $postid = get_the_ID();
    $html = str_replace('<a','<a class="label success radius"',$html);
    return $html;
}
add_filter('the_tags','add_class_the_tags',10,1);

// Enable shortcodes in widgets
add_filter('widget_text', 'do_shortcode');

// Disable jump in 'read more' link
function remove_more_jump_link($link) {
	$offset = strpos($link, '#more-');
	if ($offset) {
		$end = strpos($link, '"',$offset);
	}
	if ($end) {
		$link = substr_replace($link, '', $offset, $end-$offset);
	}
	return $link;
}
add_filter('the_content_more_link', 'remove_more_jump_link');

// Remove height/width attributes on images so they can be responsive
add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 );
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 );

function remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

// change the standard class that wordpress puts on the active menu item in the nav bar
//Deletes all CSS classes and id's, except for those listed in the array below
function custom_wp_nav_menu($var) {
        return is_array($var) ? array_intersect($var, array(
                //List of allowed menu classes
                'current_page_item',
                'current_page_parent',
                'current_page_ancestor',
                'first',
                'last',
                'vertical',
                'horizontal',
                'rss-icon'
                )
        ) : '';
}
add_filter('nav_menu_css_class', 'custom_wp_nav_menu');
add_filter('nav_menu_item_id', 'custom_wp_nav_menu');
add_filter('page_css_class', 'custom_wp_nav_menu');
 
//Replaces "current-menu-item" with "active"
function current_to_active($text){
        $replace = array(
                //List of menu item classes that should be changed to "active"
                'current_page_item' => 'active',
                'current_page_parent' => 'active',
                'current_page_ancestor' => 'active',
        );
        $text = str_replace(array_keys($replace), $replace, $text);
                return $text;
        }
add_filter ('wp_nav_menu','current_to_active');
 
//Deletes empty classes and removes the sub menu class
function strip_empty_classes($menu) {
    $menu = preg_replace('/ class=""| class="sub-menu"/','',$menu);
    return $menu;
}
add_filter ('wp_nav_menu','strip_empty_classes');


// add the 'has-flyout' class to any li's that have children and add the arrows to li's with children

class description_walker extends Walker_Nav_Menu
{
      function start_el(&$output, $item, $depth, $args)
      {
            global $wp_query;
            $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
            
            $class_names = $value = '';
            
            // If the item has children, add the dropdown class for foundation
            if ( $args->has_children ) {
                $class_names = "has-flyout ";
            }
            
            $classes = empty( $item->classes ) ? array() : (array) $item->classes;
            
            $class_names .= join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
            $class_names = ' class="'. esc_attr( $class_names ) . '"';
           
            $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

            $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
            $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
            $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
            // if the item has children add these two attributes to the anchor tag
            // if ( $args->has_children ) {
            //     $attributes .= 'class="dropdown-toggle" data-toggle="dropdown"';
            // }

            $item_output = $args->before;
            $item_output .= '<a'. $attributes .'>';
            $item_output .= $args->link_before .apply_filters( 'the_title', $item->title, $item->ID );
            $item_output .= $args->link_after;
            // if the item has children add the caret just before closing the anchor tag
            if ( $args->has_children ) {
                $item_output .= '</a><a href="#" class="flyout-toggle"><span> </span></a>';
            }
            else{
                $item_output .= '</a>';
            }
            $item_output .= $args->after;

            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
            }
            
        function start_lvl(&$output, $depth) {
            $indent = str_repeat("\t", $depth);
            $output .= "\n$indent<ul class=\"flyout\">\n";
        }
            
        function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output )
            {
                $id_field = $this->db_fields['id'];
                if ( is_object( $args[0] ) ) {
                    $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
                }
                return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
            }       
}

// Walker class to customize footer links
class footer_links_walker extends Walker_Nav_Menu
{
      function start_el(&$output, $item, $depth, $args)
      {
            global $wp_query;
            $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
            
            $class_names = $value = '';
            
            $classes = empty( $item->classes ) ? array() : (array) $item->classes;
            
            $class_names .= join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
            $class_names = ' class="'. esc_attr( $class_names ) . '"';
           
            $output .= $indent . '<li ' . $value . $class_names .'>';

            $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
            $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
            $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

            $item_output = $args->before;
            $item_output .= '<a'. $attributes .'>';
            $item_output .= $args->link_before .apply_filters( 'the_title', $item->title, $item->ID );
            $item_output .= $args->link_after;
            
            $item_output .= '</a>';
            $item_output .= $args->after;

            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
            }
            
        function start_lvl(&$output, $depth) {
            $indent = str_repeat("\t", $depth);
            $output .= "\n$indent<ul class=\"flyout\">\n";
        }
            
        function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output )
            {
                $id_field = $this->db_fields['id'];
                if ( is_object( $args[0] ) ) {
                    $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
                }
                return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
            }       
}


// Add the Meta Box to the homepage template
function add_homepage_meta_box() {  
    add_meta_box(  
        'homepage_meta_box', // $id  
        'Custom Fields', // $title  
        'show_homepage_meta_box', // $callback  
        'page', // $page  
        'normal', // $context  
        'high'); // $priority  
}  
add_action('add_meta_boxes', 'add_homepage_meta_box');

// Field Array  
$prefix = 'custom_';  
$custom_meta_fields = array(  
    array(  
        'label'=> 'Homepage tagline area',  
        'desc'  => 'Displayed underneath page title. Only used on homepage template. HTML can be used.',  
        'id'    => $prefix.'tagline',  
        'type'  => 'textarea' 
    )  
);  

// The Homepage Meta Box Callback  
function show_homepage_meta_box() {  
global $custom_meta_fields, $post;  
// Use nonce for verification  
echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';  
  
    // Begin the field table and loop  
    echo '<table class="form-table">';  
    foreach ($custom_meta_fields as $field) {  
        // get value of this field if it exists for this post  
        $meta = get_post_meta($post->ID, $field['id'], true);  
        // begin a table row with  
        echo '<tr> 
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th> 
                <td>';  
                switch($field['type']) {  
                    // text  
                    case 'text':  
                        echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="60" /> 
                            <br /><span class="description">'.$field['desc'].'</span>';  
                    break;
                    
                    // textarea  
                    case 'textarea':  
                        echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="80" rows="4">'.$meta.'</textarea> 
                            <br /><span class="description">'.$field['desc'].'</span>';  
                    break;  
                } //end switch  
        echo '</td></tr>';  
    } // end foreach  
    echo '</table>'; // end table  
}  

// Save the Data  
function save_homepage_meta($post_id) {  
    global $custom_meta_fields;  
  
    // verify nonce  
    if (isset($_POST['custom_meta_box_nonce'])) {
        if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))  
            return $post_id;  
        // check autosave  
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)  
            return $post_id;  
        // check permissions  
        if ('page' == $_POST['post_type']) {  
            if (!current_user_can('edit_page', $post_id))  
                return $post_id;  
            } elseif (!current_user_can('edit_post', $post_id)) {  
                return $post_id;  
        }  
      
        // loop through fields and save the data  
        foreach ($custom_meta_fields as $field) {  
            $old = get_post_meta($post_id, $field['id'], true);  
            $new = $_POST[$field['id']];  
            if ($new && $new != $old) {  
                update_post_meta($post_id, $field['id'], $new);  
            } elseif ('' == $new && $old) {  
                delete_post_meta($post_id, $field['id'], $old);  
            }  
        } // end foreach  
    }
}  
add_action('save_post', 'save_homepage_meta');  

//include custom post types (twitter users)
include_once('lib/post_types.php');

//include DB creation code
include_once('lib/tweets_db.php');
add_action( 'after_setup_theme', 'brt_install_tables');

//include tweets page and graph data function
include_once('lib/tweets_page.php');

//include recent tweets page 
include_once('lib/recent_tweets_page.php');

define('bitlyAccessToken', '');
define('twConsumerKey', '');
define('twConsumerSecret', '');
define('twAccessToken', '');
define('twAccessSecret', '');

/*
 * Update stats functions
 */
//Ajax function to update and display stats
add_action('wp_ajax_brt_update_tweet_stats', 'brt_update_tweet_stats' );
function brt_update_tweet_stats() {
    global $wpdb;
    $tweets_table_name = $wpdb->prefix . "tweeted_urls";
    $tweet_stats_table_name = $wpdb->prefix . "url_stats";
    $tweet_id = $_POST['tweet'];
    $short_url = $wpdb->get_row("SELECT short_url, bitly FROM $tweets_table_name WHERE `id` =$tweet_id");

    if ($short_url->bitly) {
        brt_update_stats_single($short_url->short_url, $tweet_id);
        $stats = return_stats($tweet_id);
        echo json_encode($stats);
    }
    die();
}

// function to update stats on a single tweet
function brt_update_stats_single($short_url, $tweet_id) {
    global $wpdb;
    include_once('lib/bitly.php');
    $tweet_stats_table_name = $wpdb->prefix . "url_stats";
    $tweets_table_name = $wpdb->prefix . "tweeted_urls";
    $clicks_by_day = bitly_v3_clicks_by_day($short_url);
    $track_url = of_get_option('track_url');

    if (isset($clicks_by_day[0]['clicks'])) {
        foreach ($clicks_by_day[0]['clicks'] as $clicks) {

            $post_exists = $wpdb->get_var( "SELECT * FROM $tweet_stats_table_name WHERE tweet = '".$tweet_id."' and time = '".$clicks['day_start']."';" );
            if (!$post_exists ) {
                $stats = $wpdb->get_results("INSERT INTO $tweet_stats_table_name (`tweet`, `time`, `clicks`) VALUES ( '".$tweet_id."', ".$clicks['day_start']." , ".$clicks['clicks'].") ");
            } else {
                $stats = $wpdb->get_results("UPDATE $tweet_stats_table_name SET `clicks` = '".$clicks['clicks']."' WHERE `tweet` = ".$tweet_id." and time = '".$clicks['day_start']."';");
            }
        }
    }

    $social_score = bitly_v3_link_social(bitlyAccessToken, $short_url);
    if (isset($social_score[$short_url])) {
        $sscore = $social_score[$short_url];
    } else {
        $sscore = 0;
    }

    $shared = 0;
    $global_clicks = 0;
    $local_clicks = 0;
    $retweets = 0;

    $bitly_result_aggregate = wp_remote_get("https://api-ssl.bitly.com/v3/link/encoders_count?access_token=".bitlyAccessToken."&link=" . $short_url);
    if (is_wp_error($bitly_result_aggregate) ) {
        echo "Error";
    } else {
        $data_aggregate = json_decode($bitly_result_aggregate['body']);
        if ($data_aggregate->status_code == 200) {
            $aggregate_url = $data_aggregate->data->aggregate_link;
            $aggregate_count = $data_aggregate->data->count;
            $shared = $aggregate_count;

            // get clicks on aggregate url
            $bitly_result_clicks_aggregate = wp_remote_get("https://api-ssl.bitly.com/v3/link/clicks?access_token=".bitlyAccessToken."&link=" . $aggregate_url);
            if (is_wp_error($bitly_result_clicks_aggregate) ) {
                echo "Error";
            } else {
                $data_clicks_aggregate = json_decode($bitly_result_clicks_aggregate['body']);
                if ($data_clicks_aggregate->status_code == 200) {
                    $aggregate_clicks = $data_clicks_aggregate->data->link_clicks;
                    $global_clicks = $aggregate_clicks;
                }
            }
            $bitly_result_clicks = wp_remote_get("https://api-ssl.bitly.com/v3/link/clicks?access_token=".bitlyAccessToken."&link=" . $short_url);
            if (is_wp_error($bitly_result_clicks) ) {
                echo "Error";
            } else {
                $data_clicks = json_decode($bitly_result_clicks['body']);
                if ($data_clicks->status_code == 200) {
                    $clicks_local = $data_clicks->data->link_clicks;
                    $local_clicks = $clicks_local;
                }
            }

        }
    }

// Retweets
    $tweet = $wpdb->get_var("SELECT tweet_id from $tweets_table_name where id = $tweet_id");

    $retweets_result = wp_remote_get($track_url."/tweet/" . $tweet."");
    if (is_wp_error($retweets_result) ) {
        echo "Error";
    } else {
        $retweets_data = json_decode($retweets_result['body']);
        if (isset($retweets_data->retweet_count)) {
            $retweets = $retweets_data->retweet_count;
        }
    }

    $update_query = "UPDATE $tweets_table_name SET `sscore` = '$sscore', shared = $shared, globalclicks = $global_clicks, localclicks = $local_clicks, retweets = $retweets, updated= NOW() WHERE `id` = $tweet_id;";
    $score = $wpdb->get_results($update_query);

    return true;
}

// update DB and return stats formatted to display on page
function return_stats($id) {
    global $wpdb;
    $tweets_table_name = $wpdb->prefix . "tweeted_urls";
    $tweet_stats_table_name = $wpdb->prefix . "url_stats";

    $clicks_data = $wpdb->get_row("SELECT sscore, shared, globalclicks, localclicks, retweets from $tweets_table_name where id = $id");
    $stats = "<div class='stats_$id'>";
    $stats .= "Link social score: ".$clicks_data->sscore."<br/>";
    $clicks = $wpdb->get_results("SELECT * from $tweet_stats_table_name where tweet = $id order by time desc limit 0, 5");
    if (!empty($clicks)) {
        $stats .= "Clicks on:<br/>";
        foreach ($clicks as $day) {
    //                    print_r($day);
            $stats .= "".strftime('%b %d, %Y', $day->time).": <span style='font-weight: bold;'>".$day->clicks."</span><br/>";
        }
    }
    $stats .= "</div>";
    $clicks = $wpdb->get_row();
    $data['stats'] = $stats;
    $data['shared'] = $clicks_data->shared;
    $data['globalclicks'] = $clicks_data->globalclicks;
    $data['localclicks'] = $clicks_data->localclicks;
    $data['retweets'] = $clicks_data->retweets;
    return $data;
}


//display tweet info when creating a story about the tweet
add_action('admin_init', 'brt_add_post_info');

function brt_add_post_info(){
    add_meta_box("post_info", "Tweet", "brt_tweet_post_info", "post", "normal", "core");
    wp_enqueue_script('autosave_extra', get_bloginfo('template_url').'/javascripts/autosave.js', array('jquery'), false, true);
}

function brt_tweet_post_info(){
    global $meta_box, $post, $shortname, $wpdb;
    $tweets_table_name = $wpdb->prefix . "tweeted_urls";

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post->ID;

    $action = filter_input(INPUT_GET, 'action');
    if ($action == 'edit') {
        $tweet_id = get_post_meta($post->ID, 'brt_twitter_id_post', true);
        $tweet_id_number = get_post_meta($post->ID, 'brt_twitter_id', true);
        $user_name = get_post_meta($post->ID, 'brt_twitter_user_name', true);
        $screen_name = get_post_meta($post->ID, 'brt_twitter_username', true);
        $tweet = get_post_meta($post->ID, 'brt_twitter_tweet', true);
        $short_url = get_post_meta($post->ID, 'brt_twitter_short_url', true);
        $url = get_post_meta($post->ID, 'brt_twitter_url', true);
        $newsletter_url = get_post_meta($post->ID, 'brt_twitter_newsletter_url', true);
        if (empty($newsletter_url)) {
            $newsletter_url = $short_url;
        }
        $tweet_source = get_post_meta($post->ID, 'brt_tweet_source',true);
        $impact = get_post_meta($post->ID, 'brt_impact_factor', true);
    } else {
        $tweet_id = filter_input(INPUT_GET, 'tid', FILTER_SANITIZE_NUMBER_INT);
        if ($tweet_id <= 0 || $tweet_id == '' || $action != '') {
            showMessage("No Tweet id detected, to add a story please <a href='edit.php?post_type=twuser'>select a tweet from a user</a>.", true);
            return;
        }
        $tweet_info = $wpdb->get_row("SELECT * FROM $tweets_table_name where id = $tweet_id");

        $custom = get_post_custom($post->ID);
        if (isset($custom['brt_tweet_source'])) {
          $tweet_source = $custom["brt_tweet_source"][0];
        } else {
            $host = parse_url($tweet_info->url);
          $tweet_source = $host['host'];
        }
        $tweet = $tweet_info->tweet;
        $user_name = get_the_title($tweet_info->user);
        $screen_name = get_post_meta($tweet_info->user, 'brt_twitter_username', true);
        $tweet_id_number = $tweet_info->tweet_id;
        $short_url = $tweet_info->short_url;
        $url = $tweet_info->url;
        $newsletter_url = $short_url;
        $impact = 0;
    }
?>
  <div id="brt_twitter_info">

    <table style="width: 98%;">

      <tr>
        <td>
          <label>Tweet: </label>
        </td>
        <td>
          <label>&nbsp;&nbsp;&nbsp;</label>
        </td>
        <td>
            <p><?php echo $tweet; ?></p>
        </td>
      </tr>

      <tr>
        <td>
          <label>By: </label>
        </td>
        <td>
          <label>&nbsp;&nbsp;&nbsp;</label>
        </td>
        <td>
        <?php $avatar = twitter_avatar($screen_name)?>
            <p><span style="padding: 10px 0 0;"><img style="padding:0 10px 0 0;"src="<?php echo $avatar;?>" width='48' height='48' /><?php echo $user_name; ?></span></p>
        </td>
      </tr>

      <tr>
        <td>
          <label>URLs: </label>
        </td>
        <td>
          <label>&nbsp;&nbsp;&nbsp;</label>
        </td>
        <td>
        <?php //print_r($tweet_info)?>
            <p><a href="<?php echo $tweet_info->short_url; ?>"><?php echo $short_url; ?></a> <em><-URL tweeted originally</em></p>
            <p><a href="<?php echo $tweet_info->url; ?>"><?php echo $url; ?></a></p>
            <input type="hidden" id="brt_twitter_id_post" name="brt_twitter_id_post" value="<?php echo $tweet_id; ?>"/>
            <input type="hidden" id="brt_twitter_id" name="brt_twitter_id" value="<?php echo $tweet_id_number; ?>"/>
            <input type="hidden" id="brt_twitter_username" name="brt_twitter_username" value="<?php echo $screen_name; ?>"/>
            <input type="hidden" id="brt_twitter_user_name" name="brt_twitter_user_name" value="<?php echo $user_name; ?>"/>
            <input type="hidden" id="brt_twitter_tweet" name="brt_twitter_tweet" value="<?php echo $tweet; ?>"/>
            <input type="hidden" id="brt_twitter_short_url" name="brt_twitter_short_url" value="<?php echo $short_url; ?>"/>
            <input type="hidden" id="brt_twitter_url" name="brt_twitter_url" value="<?php echo $url; ?>"/>
            <p><input id="brt_twitter_newsletter_url" name="brt_twitter_newsletter_url" class="regular-text" value="<?php echo $newsletter_url; ?>"/> <em>Change this URL to link to the direct source </em> </p> 
        </td>
      </tr>
      <tr>
        <td>
          <label>Source: </label>
        </td>
        <td>
          <label>&nbsp;&nbsp;&nbsp;</label>
        </td>
        <td>
            <input id="brt_tweet_source" name="brt_tweet_source" value="<?php echo $tweet_source; ?>"/>
        </td>
      </tr>
      <tr>
        <td>
          <label>Impact Factor: </label>
        </td>
        <td>
          <label>&nbsp;&nbsp;&nbsp;</label>
        </td>
        <td>
            <input id="brt_impact_factor" name="brt_impact_factor" value="<?php echo $impact; ?>"/>
        </td>
      </tr>
      
    </table>
  </div>
<?php
}

add_action('save_post', 'brt_update_twitter_post_info', 99);
function brt_update_twitter_post_info(){
    global $post, $wpdb;
    $tweets_table_name = $wpdb->prefix . "tweeted_urls";

    if (isset($post)) {
        if ($post->post_type != "post") {
            return;
        }
      if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){

            return $post->ID;
      } else {

        if (isset($_POST["brt_twitter_id"])) {
            $tweet_id = esc_attr($_POST["brt_twitter_id"]);
            update_post_meta($post->ID, "brt_twitter_id", $tweet_id);
        }
        if (isset($_POST["brt_twitter_username"])) {
            $tweet_username = esc_attr($_POST["brt_twitter_username"]);
            update_post_meta($post->ID, "brt_twitter_username", $tweet_username);
        }
        if (isset($_POST["brt_twitter_user_name"])) {
            $tweet_user_name = esc_attr($_POST["brt_twitter_user_name"]);
            update_post_meta($post->ID, "brt_twitter_user_name", $tweet_user_name);
        }
        if (isset($_POST["brt_twitter_tweet"])) {
            $tweet = esc_attr($_POST["brt_twitter_tweet"]);
            update_post_meta($post->ID, "brt_twitter_tweet", $tweet);
        }
        if (isset($_POST["brt_twitter_short_url"])) {
            $tweet_short_url = esc_attr($_POST["brt_twitter_short_url"]);
            update_post_meta($post->ID, "brt_twitter_short_url", $tweet_short_url);
        }
        if (isset($_POST["brt_twitter_url"])) {
            $tweet_url = esc_attr($_POST["brt_twitter_url"]);
            update_post_meta($post->ID, "brt_twitter_url", $tweet_url);
        }
        if (isset($_POST["brt_twitter_newsletter_url"])) {
            $tweet_url = esc_attr($_POST["brt_twitter_newsletter_url"]);
            update_post_meta($post->ID, "brt_twitter_newsletter_url", $tweet_url);
        }
        if (isset($_POST["brt_impact_factor"])) {
            $impact_factor = esc_attr($_POST["brt_impact_factor"]);
            update_post_meta($post->ID, "brt_impact_factor", $impact_factor);
        }
        if (isset($_POST["brt_tweet_source"])) {
            $tweet_source = esc_attr($_POST["brt_tweet_source"]);
            update_post_meta($post->ID, "brt_tweet_source", $tweet_source);
        }
        if (isset($_POST["brt_twitter_id_post"])) {
            $tweet_id_post = esc_attr($_POST["brt_twitter_id_post"]);
            update_post_meta($post->ID, "brt_twitter_id_post", $tweet_id_post);
                $wpdb->update( 
                    $tweets_table_name, 
                    array( 
                        'published' => $post->ID,  // integer
                    ), 
                    array( 'id' => $tweet_id ), 
                    array( 
                        '%d'    // published
                    ), 
                    array( '%d' ) //id
                );
        }
      } //end autosave if
    }
}

add_action('save_post', 'brt_autosave_twitter_post_info');
function brt_autosave_twitter_post_info($post){
    if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
        $post_id = $post;
        if (isset($_POST["brt_twitter_id"])) {
            $tweet_id = esc_attr($_POST["brt_twitter_id"]);
            update_metadata( 'post', $post_id, "brt_twitter_id", $tweet_id);
        }
			if (isset($_POST["brt_twitter_username"])) {
				$tweet_username = esc_attr($_POST["brt_twitter_username"]);
				update_metadata( 'post', $post_id, "brt_twitter_username", $tweet_username);
			}
			if (isset($_POST["brt_twitter_user_name"])) {
				$tweet_user_name = esc_attr($_POST["brt_twitter_user_name"]);
				update_metadata( 'post', $post_id, "brt_twitter_user_name", $tweet_user_name);
			}
			if (isset($_POST["brt_twitter_tweet"])) {
				$tweet = esc_attr($_POST["brt_twitter_tweet"]);
				update_metadata( 'post', $post_id, "brt_twitter_tweet", $tweet);
			}
			if (isset($_POST["brt_twitter_short_url"])) {
				$tweet_short_url = esc_attr($_POST["brt_twitter_short_url"]);
				update_metadata( 'post', $post_id, "brt_twitter_short_url", $tweet_short_url);
			}
			if (isset($_POST["brt_twitter_url"])) {
				$tweet_url = esc_attr($_POST["brt_twitter_url"]);
				update_metadata( 'post', $post_id, "brt_twitter_url", $tweet_url);
			}
            if (isset($_POST["brt_twitter_newsletter_url"])) {
                $tweet_url = esc_attr($_POST["brt_twitter_newsletter_url"]);
                update_post_meta($post->ID, "brt_twitter_newsletter_url", $tweet_url);
            }
			if (isset($_POST["brt_impact_factor"])) {
				$impact_factor = esc_attr($_POST["brt_impact_factor"]);
				update_metadata( 'post', $post_id, "brt_impact_factor", $impact_factor);
			}
			if (isset($_POST["brt_tweet_source"])) {
				$tweet_source = esc_attr($_POST["brt_tweet_source"]);
				update_metadata( 'post', $post_id, "brt_tweet_source", $tweet_source);
			}
			if (isset($_POST["brt_twitter_id_post"])) {
				$tweet_id_post = esc_attr($_POST["brt_twitter_id_post"]);
				update_metadata( 'post', $post_id, "brt_twitter_id_post", $tweet_id_post);
			}
    }
}

function brt_newsletter_get_post() {
    global $random;
    global $wpdb;
    
    $newsletter_post_data = get_post(filter_input(INPUT_POST, 'post_id', FILTER_SANITIZE_NUMBER_INT));

    $data_post ="";
    $post_link = get_permalink($newsletter_post_data->ID);
    if ($newsletter_post_data) {
        $tweet_id = get_post_meta($newsletter_post_data->ID, 'brt_twitter_id_post', true);
        if ($tweet_id) {
            $tweets_table_name = $wpdb->prefix . "tweeted_urls";

            $tweet = $wpdb->get_row("SELECT * from $tweets_table_name where id = $tweet_id");

        }
        if ($tweet) {
            $link = $tweet->short_url;
            $target = "_blank";
        } else {
            $link = get_permalink();
            $target = "_self";
        }
        if (has_post_thumbnail($newsletter_post_data->ID)) {
            $img_src = get_the_post_thumbnail( $newsletter_post_data->ID, 'bones-thumb-161' );
        } else {
            $img_src = '<img src="'.get_bloginfo('template_url').'/images/default_nl.jpg" width="161" height="129"/>';
        }

        if (strlen($newsletter_post_data->post_title) > 45 ) {
            if (strlen($newsletter_post_data->post_content) > 240 ) {
                $content = substr($newsletter_post_data->post_content, 0, 240)."&hellip;";
            } else {
                $content = substr($newsletter_post_data->post_content, 0, 240);
            }
        } else {
            if (strlen($newsletter_post_data->post_content) > 295 ) {
                $content = substr($newsletter_post_data->post_content, 0, 295)."&hellip;";
            } else {
                $content = substr($newsletter_post_data->post_content, 0, 295);
            }
        }
//5 rows = 325
//4 rows = 240
$user_name = get_post_meta($newsletter_post_data->ID, 'brt_twitter_user_name', true);
$screen_name = get_post_meta($newsletter_post_data->ID, 'brt_twitter_username', true);
$impact = get_post_meta($newsletter_post_data->ID, 'brt_impact_factor', true);

$source = get_post_meta($newsletter_post_data->ID, 'brt_tweet_source', true);

$newsletter_url = get_post_meta($newsletter_post_data->ID, 'brt_twitter_newsletter_url', true);
if (empty($newsletter_url)) {
    $newsletter_url = $link;
}

if ($tweet) {
    $tweet_id_number = $tweet->tweet_id;
    $tooltip = "$tweet->globalclicks Global clicks<br/>$tweet->localclicks Brain trust clicks<br/>$tweet->shared Shares<br/>$tweet->retweets RTs";
} else {
    $tweet_id_number = get_post_meta($newsletter_post_data->ID, 'brt_twitter_id', true);
    $tweet = get_post_meta($newsletter_post_data->ID, 'brt_twitter_tweet', true);
    $link = get_post_meta($newsletter_post_data->ID, 'brt_twitter_short_url', true);
    $tooltip = "";
}
$path = get_bloginfo('template_url');
$avatar = twitter_avatar($screen_name );
$data_post = <<<DATAPOST
<table border="0" cellpadding="0" cellspacing="0" width="100%"  class='section'>
    <tr>
        <td rowspan="2" valign='top'>
                    <a href="{$newsletter_url}" title="{$newsletter_post_data->post_title}" target="_blank">{$img_src}</a>
        </td>
        <td class='default_image'>
                    <h2><a href="{$newsletter_url}" rel="bookmark" title="{$newsletter_post_data->post_title}" target="_blank">{$newsletter_post_data->post_title}</a></h2>
                    <p class="meta"><a href="{$newsletter_url}" title="{$newsletter_post_data->post_title}" target="_blank">{$source}</a></p>
        </td>
    </tr>
    <tr>
        <td>
                <div class="post_content">
                    <p>{$content}</p>
            
                </div>
        </td>
    </tr>
    <tr>
        <td colspan='2'>
                <div class="footer">
                    <div class="tinfo">
                        <table>
                            <tr>
                                <td width="50">
                        <a href="https://twitter.com/{$screen_name}/status/{$tweet_id_number}" target="_blank"><img style=\"padding:0 10px 0 0;\" src="{$avatar}" width='48' height='48' alt="{$user_name}"/></a>
                                </td>
                                <td background="https://gallery.mailchimp.com/755fd73cd4524c700cc61d1ab/images/impact_back_email1.jpg" bgcolor="#ffffff" width="40" height="40" valign="middle" align="center">
                                  <!--[if gte mso 9]>
  <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:45px;height:45px;">
    <v:fill type="tile" src="https://gallery.mailchimp.com/755fd73cd4524c700cc61d1ab/images/impact_back_email1.jpg" color="#ffffff" />
    <v:textbox inset="0,0,0,0">
  <![endif]-->
  <div>
                       <span class='impact'>{$impact}</span> 
  </div>
  <!--[if gte mso 9]>
    </v:textbox>
  </v:rect>
  <![endif]-->
</td>
                                <td width="55">
                                </td>
                                <td>
                        <a class="share" rel="nofollow" title="Email" href="{$post_link}email/">Email</a>
                        <a class="share" target="_blank" href="https://twitter.com/intent/tweet?url={$link}">Tweet</a>
                        <a class="share" target="_blank" href="https://plus.google.com/share?url={$link}">G+</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div> 
        </td>
    </tr>
</table>
DATAPOST;

    }
    echo $data_post;
    die();
}

function brt_newsletter_get_post_old() {
    global $random;
    global $wpdb;
    
    $newsletter_post_data = get_post(filter_input(INPUT_POST, 'post_id', FILTER_SANITIZE_NUMBER_INT));

    $data_post ="";
    $post_link = get_permalink($newsletter_post_data->ID);
    if ($newsletter_post_data) {
        $tweet_id = get_post_meta($newsletter_post_data->ID, 'brt_twitter_id_post', true);
        if ($tweet_id) {
            $tweets_table_name = $wpdb->prefix . "tweeted_urls";

            $tweet = $wpdb->get_row("SELECT * from $tweets_table_name where id = $tweet_id");

        }
        if ($tweet) {
            $link = $tweet->short_url;
            $target = "_blank";
        } else {
            $link = get_permalink();
            $target = "_self";
        }
        if (has_post_thumbnail($newsletter_post_data->ID)) {
            $img_src = get_the_post_thumbnail( $newsletter_post_data->ID, 'bones-thumb-130' );
        } else {
            $img_src = '<img src="'.get_bloginfo('template_url').'/images/default.jpg" width="129" height="82"/>';
        }
$user_name = get_the_title($tweet->user);
$screen_name = get_post_meta($tweet->user, 'brt_twitter_username', true);
$source = get_post_meta($newsletter_post_data->ID, 'brt_tweet_source', true);
$tooltip = "$tweet->globalclicks Global clicks - $tweet->localclicks Brain trust clicks - $tweet->shared Shares - $tweet->retweets RTs";
$path = get_bloginfo('template_url');
$avatar = twitter_avatar($screen_name );
$data_post = "
<div class=\"section\">
<div class=\"default_image\">
    <table>
        <tr>
            <td width=\"85\">
                <a href=\"{$link}\" title=\"{$newsletter_post_data->post_title}\" target=\"_blank\">{$img_src}</a>
            </td>
            <td>
    <h2><a href=\"{$link}\" rel=\"bookmark\" title=\"{$newsletter_post_data->post_title}\" target=\"_blank\">{$newsletter_post_data->post_title}</a></h2>
    <p class=\"meta\"><a href=\"{$link}\" title=\"{$newsletter_post_data->post_title}\" target=\"_blank\">{$source}</a></p>
            </td>
        </tr>
    </table>
</div>
<div class=\"post_content\">
<p>{$newsletter_post_data->post_content}</p>

</div>
<div class=\"footer\">
    <div class=\"tinfo\">
        <table>
            <tr>
                <td width=\"360\">
                    <a href=\"https://twitter.com/{$screen_name}/status/{$tweet->tweet_id}\" target=\"_blank\"><img style=\"padding:0 10px 0 0;\" src=\"{$avatar}\" width=\"48\" height=\"48\" alt=\"{$user_name}\"></a>
                    <img src=\"{$path}/images/impact.jpg\" width=\"40\" height=\"40\" class=\"has-tip tip-right\" title=\"{$tooltip}\">
                    <img src=\"{$path}/images/kinetics1.png\" width=\"40\" height=\"40\" />
                </td>
                <td width=\"230\">
                    <a class=\"share\" rel=\"nofollow\" title=\"Email\" href=\"{$post_link}email/\">Email</a>
                    <a class=\"share\" target=\"_blank\" href=\"https://twitter.com/intent/tweet?url={$link}\">Tweet</a>
                    <a class=\"share\" target=\"_blank\" href=\"https://plus.google.com/share?url={$link}\">G+</a>
                </td>
            </tr>
        </table>
    </div>
</div> 
</div>
";
    }
    echo $data_post;
    die();
}

add_action( 'wp_ajax_wordchimp_get_post', 'brt_newsletter_get_post' );
add_action( 'wp_ajax_newsletter_get_post', 'brt_newsletter_get_post' );

function brt_newsletter_get_avatars() {
    $args = array(  
        'post_type' => 'twuser',
        'numberposts' => 10,
        'orderby'=>'rand', 
    );
    $avatars = get_posts( $args );
    $html_avatars = "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
        <tr>";
    $cell = 0;
    foreach ($avatars as $avatar) {
        $screen_name = get_post_meta($avatar->ID, 'brt_twitter_username', true);
        $user_name = str_replace(' ', '<br/>', $avatar->post_title);
        $avatar = twitter_avatar($screen_name );
        $html_avatar = "
        <td>
        <div class=\"avatar\">
            <img src=\"{$avatar}\" style=\"max-width:48px; max-height:48px;\"> 
            <p class=\"name\">{$user_name}</p>
            </div>
        </td>";
        $html_avatars .= $html_avatar;
        $cell++;
        if ($cell == 5) {
            $html_avatars .= "</tr><tr>";
        }
    }
    $html_avatars .= "</tr>
    </table>";
    return $html_avatars;
}

/*
 * Utility functions
 */
// add link to the feed in the menu
add_filter( 'wp_nav_menu_items', 'feed_menu_item', 10, 2 );
function feed_menu_item ( $items, $args ) {
    if ( $args->theme_location == 'main_nav') {
        $feed_url = of_get_option('feed_url');
        if ($feed_url) {
            $items .= '<li class="rss-icon"><a href="'.$feed_url.'">RSS</a></li>';
        } else {
            $items .= '<li class="rss-icon"><a href="'.get_bloginfo('rss2_url').'">RSS</a></li>';
        }
    }
    return $items;
}

// display a message (error/update) in the admin backend
add_action('admin_notices', 'showMessage');
function showMessage($message, $errormsg = false) {
    if (!$message) 
        return;
    if ($errormsg) {
        echo '<div id="message" class="error">';
    } else {
        echo '<div id="message" class="updated fade">???';
    }

    echo "<p><strong>$message</strong></p></div>";
}

//include cron settings and functions
/** Setup cron schedule **/
add_filter('cron_schedules','brt_cron_add_schedules');

function brt_cron_add_schedules($schedules) {
    $schedules['2min'] = array('interval' => 60, 'display' => 'every minute');
    $schedules['5min'] = array('interval' => 300, 'display' => 'every 5 minutes');
    $schedules['15min'] = array('interval' => 900, 'display' => 'every 15 minutes');
    $schedules['30min'] = array('interval' => 1800, 'display' => 'every 30 minutes');
    return $schedules;
}

function brt_cron_settings() {
    if (!wp_next_scheduled('brt_cron_update_tweets')) {
        wp_schedule_event( time(), '5min', 'brt_cron_update_tweets' );
    }
    if (!wp_next_scheduled('brt_cron_update_tweet_stats')) {
        wp_schedule_event( time()+120, '15min', 'brt_cron_update_tweet_stats' );        
    }
    if (!wp_next_scheduled('brt_cron_delete_old_tweet_stats')) {
        wp_schedule_event( time()+180, 'daily', 'brt_cron_delete_old_tweet_stats' );        
    }
    if (!wp_next_scheduled('brt_cron_delete_old_tweets')) {
        wp_schedule_event( time()+240, 'daily', 'brt_cron_delete_old_tweets' );        
    }
}

/** Schedule tweets updates **/
add_action('brt_cron_update_tweets', 'brt_update_tweets');
/** Schedule tweet stats updates **/
add_action('brt_cron_update_tweet_stats', 'brt_update_stats');
/** Schedule tweets updates **/
add_action('brt_cron_delete_old_tweets', 'brt_delete_old_tweets');
/** Schedule tweet stats updates **/
add_action('brt_cron_delete_old_tweet_stats', 'brt_delete_old_tweet_stats');

function brt_update_tweets() {
    global $wpdb;
    $table_name = $wpdb->prefix . "tweeted_urls";

    date_default_timezone_set('America/Denver');

    require 'lib/tmhOAuth.php';
    require 'lib/tmhUtilities.php';
    include_once('lib/bitly.php');
    $tmhOAuth = new tmhOAuth(array(
      'consumer_key' => twConsumerKey,
      'consumer_secret' => twConsumerSecret,
      'user_token' => twAccessToken,
      'user_secret' => twAccessSecret,
    ));

    //Twitter users
    $args = array( 
        'post_type' => array('twuser'),
    'numberposts' => 5,
    'order' => 'ASC',
    'orderby' => 'meta_value',
    'meta_key' => 'brt_last_checked'
  //Previous settings
//        'numberposts' => 2,
//        'orderby' => 'rand',
        );
    $twitter_users = get_posts( $args );
    $usernames = array();
    foreach ($twitter_users as $username) {
        $user_name = get_post_meta( $username->ID, 'brt_twitter_username' , true );
        $last_id = get_post_meta( $username->ID, 'brt_last_tweet_id' , true );
        $usernames[] = array('id' => $username->ID, 'username' => $user_name, 'last_id' => $last_id);
    }

    $counter = 0;
    foreach ($usernames as $user) {
    echo "<h2>".$user['username']." last saved id: ".$user['last_id']."</h2>\n";

        $last_saved_id = $user['last_id'];
        
        if ($last_saved_id) {
            //request tweets since last id
            echo "requesting from last saved id:$last_saved_id<br/>";
            $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/statuses/user_timeline'), array(
              'include_entities' => '1',
              'include_rts' => '1',
              'screen_name' => $user['username'],
              'count' => 100,
              'since_id' => $last_saved_id
            ));
        } else {
            //get last 100 tweets
            echo "requesting latest 100<br/>";
            $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/statuses/user_timeline'), array(
              'include_entities' => '1',
              'include_rts' => '1',
              'screen_name' => $user['username'],
              'count' => 100,
            ));
        }

        if ($code == 200) {
            $timeline = json_decode($tmhOAuth->response['response'], true);
            foreach ($timeline as $tweet) {
                $urls = array();
                foreach ($tweet['entities'] as $type => $things) {
                    foreach ($things as $entity => $value) {
                        switch ($type) {
                            case 'urls':
                                $urls[] = empty($value['expanded_url']) ? $value['url'] : $value['expanded_url'];
                                break;
                        }
                    }
                }

                $entified_tweet = tmhUtilities::entify_with_options($tweet);
                $is_retweet = isset($tweet['retweeted_status']);


                foreach($urls as $url) {

                    $post_exists = $wpdb->get_row( $wpdb->prepare("SELECT * FROM $table_name WHERE tweet_id = '%s' and short_url = '%s';",$tweet['id_str'], $url));
                    $bitly_url = 0;
                    if ($post_exists == null ) {
                        date_default_timezone_set('America/Denver');

                        $long_url = bitly_v3_expand($url);
                        if (isset($long_url[0]['long_url']) && $url != $long_url[0]['long_url']) {
                            $bitly_url = 1;
                            $expanded_url = $long_url[0]['long_url'];
                        } else {
                            $expanded_url = "";
                        }
                        //only save if is a bitly URL
                        if ($bitly_url == 1) {
                            $wpdb->insert( 
                                $table_name, 
                                array( 
                                    'user' => $user['id'], 
                                    'tweet_id' => $tweet['id_str'], 
                                    'time' => date('Y-m-d H:i:s', strtotime($tweet['created_at'])), 
                                    'tweet' => $tweet['text'], 
                                    'short_url' => $url, 
                                    'url' => $expanded_url,
                                    'bitly' => $bitly_url
                                ), 
                                array( 
                                    '%d',
                                    '%s', 
                                    '%s', 
                                    '%s', 
                                    '%s',
                                    '%s',
                                    '%d' 
                                ) 
                            );
                        } else {
                            $wpdb->insert( 
                                $table_name, 
                                array( 
                                    'user' => $user['id'], 
                                    'tweet_id' => $tweet['id_str'], 
                                    'time' => date('Y-m-d H:i:s', strtotime($tweet['created_at'])), 
                                    'tweet' => $tweet['text'], 
                                    'short_url' => $url, 
                                    'url' => $url,
                                    'bitly' => $bitly_url
                                ), 
                                array( 
                                    '%d',
                                    '%s', 
                                    '%s', 
                                    '%s', 
                                    '%s',
                                    '%s',
                                    '%d' 
                                ) 
                            );
                        }

                        if ($bitly_url== 1 && $wpdb->insert_id) {
                            brt_update_stats_single($url, $wpdb->insert_id);
                        }
                        $counter++;
                    } else {
//                        echo "<br/>Skipping this post (already saved)";
                    }
                }

            }
        //update post meta for each user
        update_post_meta( $user['id'], 'brt_last_tweet_id', $tweet['id_str']); 
        update_post_meta($user['id'], 'brt_last_checked', time());
        } else {
          tmhUtilities::pr($tmhOAuth->response);
        }

    }
}

function brt_update_stats() {
    global $wpdb;
    $counter = 0;
    include_once('lib/bitly.php');

    $tweets_table_name = $wpdb->prefix . "tweeted_urls";
    $tweet_stats_table_name = $wpdb->prefix . "url_stats";

    $get_tweets = $wpdb->get_results("SELECT * FROM $tweets_table_name where time > '".date('Y-m-d G:i:s', strtotime('-7 days')) ."' and updated between '".date('Y-n-j G:i:s', time()-7200) ."' and '".date('Y-n-j G:i:s', time())."' or updated = '0000-00-00 00:00:00' ORDER BY RAND( ) limit 0,10");

    foreach ( $get_tweets as $tweet ) {
        $clicks_by_day = bitly_v3_clicks_by_day($tweet->short_url);
        if (isset($clicks_by_day[0]['clicks'])) {
            foreach ($clicks_by_day[0]['clicks'] as $clicks) {
                $post_exists = $wpdb->get_var( "SELECT * FROM $tweet_stats_table_name WHERE tweet = '".$tweet->id."' and time = '".$clicks['day_start']."';" );
                if (!$post_exists ) {
                    $stats = $wpdb->get_results("INSERT INTO $tweet_stats_table_name (`tweet`, `time`, `clicks`) VALUES ( '".$tweet->id."', ".$clicks['day_start']." , ".$clicks['clicks'].") ");
                } else {
                    $stats = $wpdb->get_results("UPDATE $tweet_stats_table_name SET `clicks` = '".$clicks['clicks']."' WHERE `tweet` = ".$tweet->id." and time = '".$clicks['day_start']."';");
                }
                $counter++;
            }
        }

        $social_score = bitly_v3_link_social(bitlyAccessToken, $tweet->short_url);
        if (isset($social_score[$tweet->short_url])) {
            $sscore = $social_score[$tweet->short_url];
        } else {
            $sscore = 0;
        }
        if ($tweet->short_url == $tweet->url) {
            $long_url = bitly_v3_expand($tweet->short_url);
            if (isset($long_url[0]['long_url'])) {
                $url = $long_url[0]['long_url'];
                $update_query = "UPDATE $tweets_table_name SET `sscore` = '$sscore', url='$url', bitly=1, updated= NOW() WHERE `id` = $tweet->id;";
            } else {
                $update_query = "UPDATE $tweets_table_name SET `sscore` = '$sscore', bitly=0, updated= NOW() WHERE `id` = $tweet->id;";
            }
        } else {
            $update_query = "UPDATE $tweets_table_name SET `sscore` = '$sscore', updated= NOW() WHERE `id` = $tweet->id;";
        }
        $score = $wpdb->get_results($update_query);
    }
}

function brt_delete_old_tweets() {
    global $wpdb;
    $table_name = $wpdb->prefix . "tweeted_urls";
    $query = "DELETE FROM $table_name WHERE time < curdate( ) - INTERVAL DAYOFWEEK( curdate( ) ) +0 DAY";
    $wpdb->query($query);

}

function brt_delete_old_tweet_stats() {
    global $wpdb;
    $table_name = $wpdb->prefix . "url_stats";
    $query = "DELETE FROM $table_name WHERE TO_DAYS( FROM_UNIXTIME( CAST( time AS UNSIGNED ) ) ) < TO_DAYS( FROM_UNIXTIME( UNIX_TIMESTAMP( ) ) ) -4";
    $wpdb->query($query);
}

add_action( 'after_setup_theme', 'brt_cron_settings' );

function twitter_avatar($twitter_username) {
    $avatar = false;
    $avatar = get_option($twitter_username.'_avatar');
    if ($avatar === false) {
        require_once('lib/twitterapiexchange.php');
        
        $settings = array(
            'oauth_access_token' => twAccessToken,
            'oauth_access_token_secret' => twAccessSecret,
            'consumer_key' => twConsumerKey,
            'consumer_secret' => twConsumerSecret
        );

        $url = 'https://api.twitter.com/1.1/users/show.json';
        $getfield = '?screen_name='.$twitter_username;
        $requestMethod = 'GET';
        $twitter = new TwitterAPIExchange($settings);
        $json = $twitter->setGetfield($getfield)
                     ->buildOauth($url, $requestMethod)
                     ->performRequest();  

        $result_username = json_decode($json);

    // Access the profile_image_url element in the array
        if (isset($result_username->errors[0]) && $result_username->errors[0]->code === 88 ) {
            $avatar = '';
        } else {
            $avatar = $result_username->profile_image_url;
            //save avatar transient
            update_option( $twitter_username.'_avatar', $avatar);
        } 
    } else {
    }
    return $avatar;
}

function embed_tweet($twitter_username, $tweet_id) {
    $embed_code = false;
    $embed_code = get_option($tweet_id.'_embedcode');

    if ($embed_code === false ) {
        $url = "https://twitter.com/".$twitter_username."/statuses/".$tweet_id;
        // do we need the JS to display the css?
        // uncomment to remove it 
        //$args = array("omit_script" => 'true'); 
        $args = array();
        $embed_code = wp_oembed_get( $url, $args );
        update_option( $tweet_id.'_embedcode', $embed_code);
    }
    return $embed_code;
}

add_shortcode('braintrust', 'brt_show_braintrust');

function brt_show_braintrust($attr) {

    $show = -1;

    if (isset($attr['show']) && intval($attr['show']) > 0 ) {
        $show = intval($attr['show']);
    }

    $args = array(  
        'post_type' => 'twuser',
        'numberposts' => $show,
        'orderby'   => 'rand'
        );
    $avatars = get_posts($args);
    $featured_thumb_args = array(
            'class' => "clearfix",
        );
    $last = sizeof($avatars);
    $last_class = "";
    if (!empty($avatars)) {
        $count = 0;
        $braintrust_result = "<div id=\"braintrust\">\n<ul id=\"avatars\" class=\"twelve columns\">";
        foreach( $avatars as $post ) :  setup_postdata($post); 
            if ($count == $last-1) { $last_class = " end"; }
            $user_name = str_replace(' ', '<br/>', get_the_title($post->ID));
            $screen_name = get_post_meta($post->ID, 'brt_twitter_username', true);
            $avatar = twitter_avatar($screen_name );

            $braintrust_result .= braintrust_avatar($post->ID, 'two columns mobile-two', $last_class);

            $count++;
        endforeach;
    }  
    $braintrust_result .= "</u>\n</div>\n";
    return $braintrust_result;
}

function braintrust_avatar($id, $class = "", $last_class="") {
    $braintrust_avatar = "";
    $title_name = get_the_title($id);
    $user_name = str_replace(' ', '<br/>', $title_name);
    $screen_name = get_post_meta($id, 'brt_twitter_username', true);
    $avatar = twitter_avatar($screen_name );

    $braintrust_avatar .= "<li class=\"$class $last_class\">\n";
    $braintrust_avatar .= "<a href=\"https://twitter.com/$screen_name/\" target=\"_blank\">\n";
    $braintrust_avatar .= "<img class=\"people\" src=\"$avatar\" width='48' height='48' alt=\"$user_name\" title=\"$title_name\"/>\n";
    $braintrust_avatar .= "<p>$user_name</p>\n";
    $braintrust_avatar .= "</a>\n";
    $braintrust_avatar .= "</li>\n";
    return $braintrust_avatar;
}

function add_google_analytics() {
    $ga_tracking_id = of_get_option('ga_id');
    if (isset($ga_tracking_id) && !empty($ga_tracking_id)) {
?>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<?php echo $ga_tracking_id; ?>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<?php
    }
}

add_action('wp_footer', 'add_google_analytics');

if (is_admin()) {
    include('library/newsletter.php');
}
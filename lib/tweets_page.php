<?php
/*
 * Page to display tweets from the selected user
 */

define('bitlyAccessToken', '');

add_action('admin_menu', 'register_custom_menu_page');

function register_custom_menu_page() {
    add_menu_page( 'Tweets info', 'Tweets info', 'edit_posts', 'tweets_info', 'tweet_info');
}

function tweet_info() {
    wp_enqueue_script( 'update_stats', get_template_directory_uri() . '/javascripts/update-stats.js', array('jquery') );
    wp_enqueue_script( 'colorbox', get_template_directory_uri().'/javascripts/jquery.colorbox-min.js', array('jquery') );
    wp_enqueue_style('colorbox', get_template_directory_uri().'/stylesheets/colorbox.css');
    wp_enqueue_script( 'dygraph', get_template_directory_uri().'/javascripts/dygraph-combined.js', array('jquery') );
 ?>
 <script type="text/javascript">
     var loading_img = '<?php echo get_bloginfo('template_url'); ?>/images/ajax_loader.gif';
 </script>
    <div class="wrap">
        
        <div id="icon-users" class="icon32"><br/></div>
        <h2>Tweets info</h2>

<?php if ( isset($_GET['tuid']) && (intval($_GET['tuid'] > 0) ) ) { 

    include_once('tweets-table.php');

    $TweetsListTable = new Tweets_List_Table();

    //Fetch, prepare, sort, and filter our data...
    $TweetsListTable->prepare_items($_GET['tuid']);
    $screen_name = get_post_meta($_GET['tuid'], 'brt_twitter_username', true);

    $avatar = twitter_avatar($screen_name);
?>
        <p style="font-size: 1.7rem; margin: 30px 0 0;">Tweets from: <span style="font-weight: bold;display: block;padding: 10px 0 0;"><img style="padding:0 10px 0 0;"src="<?php echo $avatar; ?>" width='48' height='48' /><?php echo get_the_title($_GET['tuid']); ?></span></p>
        <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
        <form id="tweets-filter" method="get">
            <!-- For plugins, we also need to ensure that the form posts back to our current page -->
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <!-- Now we can render the completed list table -->
            <?php $TweetsListTable->display() ?>
        </form>
        <div style='display:none'>
            <div id='inline_content' style='padding:10px; background:#fff;'>
            <p>This data is retrieved from Bitly on each request.</p>
<!--            <figure style="width: 95%; height: 300px; margin: 0 auto;" id="myChart"></figure> -->
<div id="graph" style="height: 356px; margin: 0 auto; width: 90%;"></div>
            </div>
        </div>
<?php } else { ?>
        <div style="background:#ECECEC;border:1px solid #CCC;padding:0 10px;margin-top:5px;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;">
            <p>No user selected</p> 
            <p>Please go to the <tt><a href="http://dev.local/bitlyrt/wp-admin/edit.php?post_type=twuser" style="text-decoration:none;">list of users</a></tt> and click on the 'View tweets' link of the user you want to see the tweets.</p>
        </div>
<?php } ?>
    </div>
    <?php

}


/*
 * Function to display stats from the tweet on graph
 */
add_action('wp_ajax_brt_xhour', 'brt_data_xhour');
function brt_data_xhour() {
    global $wpdb;
    $tweets_table_name = $wpdb->prefix . "tweeted_urls";
    $track_url = of_get_option('track_url');

    $tweet_id = $_POST['tweet'];
    $time = intval($_POST['time']);
    $tweet = $wpdb->get_row("SELECT short_url, tweet_id FROM $tweets_table_name WHERE `id` =$tweet_id");

//Clicks
    $bitly_result = wp_remote_get("https://api-ssl.bitly.com/v3/link/clicks?access_token=".bitlyAccessToken."&link=" . $tweet->short_url."&unit=hour&units=$time&rollup=false&format=json");
    if (is_wp_error($bitly_result) ) {
        echo "Error";
    } else {
        $data = json_decode($bitly_result['body']);

        $bitly_global = wp_remote_get("https://api-ssl.bitly.com/v3/info?access_token=".bitlyAccessToken."&shortUrl=".$tweet->short_url."&format=json");
        if (is_wp_error($bitly_global) ) {
            echo "Error retrieving clicks";
        } else {
            $data_global = json_decode($bitly_global['body']);
            $global_hash = $data_global->data->info[0]->global_hash;

//Global clicks
            $bitly_global_result = wp_remote_get("https://api-ssl.bitly.com/v3/link/clicks?access_token=".bitlyAccessToken."&link=http://bit.ly/" . $global_hash."&unit=hour&units=$time&rollup=false&format=json");
            if (is_wp_error($bitly_global_result) ) {
                echo "Error retrieving global clicks";
            } else {
                $data_clicks_global = json_decode($bitly_global_result['body']);
            }
        }
//Shares
        $bitly_shares_result = wp_remote_get("https://api-ssl.bitly.com/v3/link/shares?access_token=".bitlyAccessToken."&link=" . $tweet->short_url."&unit=hour&units=$time&rollup=false&format=json");
        if (is_wp_error($bitly_shares_result) ) {
            echo "Error retrieving shares";
        } else {
            $shares = json_decode($bitly_shares_result['body']);
        }

//Retweets
        $retweets_result = wp_remote_get($track_url."/retweets/" . $tweet->tweet_id."");
        if (is_wp_error($retweets_result) ) {
            echo "Error";
        } else {
            $retweets = json_decode($retweets_result['body']);

            $rt_count = array();
            foreach ($retweets as $retweet) {
                $rt_at = intval( substr($retweet->_id, 0,8), 16 );
                $prev = $rt_at - ($rt_at % 3600);
                if (array_key_exists($prev, $rt_count)) {
                    $rt_count[$prev]++; 
                } else {
                    $rt_count[$prev] = 1; 
                }
            }
        }

        if ($data->status_code == 200) {
            $clicks_by_hour = $data->data->link_clicks;
            $global_by_hour = $data_clicks_global->data->link_clicks;
            $shares_by_hour = $shares->data->shares;
            $retweets_by_hour = $rt_count;

            $points_data = '';
            krsort($clicks_by_hour);
            krsort($global_by_hour);

            $index = 47;
            foreach ($clicks_by_hour as $day) {

                //integrate shares
                $shares_num = 0;
                foreach ($shares_by_hour as $shares_day) {
                    if ($shares_day->dt == $day->dt) {
                        $shares_num = $shares_day->values[0]->shares;
                    }
                }

                //integrate retweets
                $retweets_num = 0;
                foreach ($retweets_by_hour as $time => $retweets) {
                    if ($time == $day->dt) {
                        $retweets_num = $retweets;
                    }
                }

                $points_data .= date('Y-m-d H:i:s',$day->dt).",".$day->clicks.",".$global_by_hour[$index]->clicks.",".$shares_num.",".$retweets_num."\n";
                $index--;
            }
            echo json_encode($points_data);
            die();
        }
    }
}

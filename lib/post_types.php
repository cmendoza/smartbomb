<?php

/*
 * Twitter users custom post type.
 */
define('twConsumerKey', '');
define('twConsumerSecret', '');
define('twAccessToken', '');
define('twAccessSecret', '');

add_action('init', 'brt_create_twitter_user_cpt');

function brt_create_twitter_user_cpt() {
  $paged = true;
  $labels = array(
    'name' => _x('Twitter users', 'About post type'),
    'singular_name' => _x('Twitter User Entry', 'About post type singular name'),
    'add_new' => _x('Add New Twitter user', 'lensvelt_robot'),
    'add_new_item' => __('Add New Twitter user'),
    'edit_item' => __('Edit Twitter user'),
    'new_item' => __('New Twitter user'),
    'view_item' => __('View Twitter user'),
    'search_items' => __('Search Twitter users'),
    'not_found' =>  __('No items found'),
    'not_found_in_trash' => __('No items found in Trash'), 
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => false,
    'show_ui' => true,
    '_builtin' => false,
    'rewrite' => array('slug'=>'about','with_front'=>true),
    'capability_type' => 'post',
    'hierarchical' => false,
    'has_archive' => false,
    'show_in_nav_menus'=> false,
    'query_var' => true,
    'paged' => $paged,      
    'menu_position' => 5,
    'supports' => array('title', 'thumbnail')
  ); 
  register_post_type('twuser',$args);

  register_taxonomy("twuser_entries", 
            array("twuser"), 
            array(  "hierarchical" => true, 
                "label" => "Twitter user Categories", 
                "singular_label" => "Twitter user Categories", 
                'rewrite' => array('slug' => 'twuser-category'),
                "query_var" => true,
              'paged' => $paged
              ));  
  flush_rewrite_rules( false ); 
}

/*
 * Twitter user URL box
 */
add_action('admin_init', 'brt_add_twitter_info');
  
add_action('save_post', 'brt_update_twitter_info');

function brt_add_twitter_info(){
    add_meta_box("twitter_url", "Twitter User", "brt_twitter_url", "twuser", "normal", "core");
}
function brt_twitter_url(){
    global $meta_box, $post, $shortname;
    
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return $post->ID;
    
    $custom = get_post_custom($post->ID);
    if (isset($custom['brt_twitter_username'])) {
      $brt_twitter_username = $custom["brt_twitter_username"][0];
    } else {
      $brt_twitter_username = '';
    }
?>
  <div id="brt_twitter_username">
  
    <table style="width: 98%;">
      <tr>
        <td>
          <label>Username: </label>
        </td>
        <td>
          <label>&nbsp;&nbsp;&nbsp;</label>
        </td>
        <td>
          <input class="regular-text" name="brt_twitter_username" id="brt_twitter_username" value="<?php echo $brt_twitter_username; ?>" />
        </td>
      </tr>
      
    </table>
  </div>
<?php
}

function brt_update_twitter_info(){
    global $post;
    $track_url = of_get_option('track_url');

    if (isset($post)) {
        if ($post->post_type != "twuser") {
            return;
        }
      if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
        return $post->ID;
      } else {
        require 'tmhOAuth.php';
        require 'tmhUtilities.php';
        $tmhOAuth = new tmhOAuth(array(
          'consumer_key' => twConsumerKey,
          'consumer_secret' => twConsumerSecret,
          'user_token' => twAccessToken,
          'user_secret' => twAccessSecret,
        ));
        if (isset($_POST["brt_twitter_username"])) {
            $username = esc_attr($_POST["brt_twitter_username"]);
            // add twitter user to db
            $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/users/show'), array(
              'include_entities' => 'false',
              'screen_name' => $username
            ));
            $user_data = json_decode($tmhOAuth->response['response'], true);
            if (!empty($user_data['id_str'])) {
                $id_str = $user_data['id_str'];
                $url = $track_url."/follow";
                $post_data = array('id'=> intval($id_str));
                $response = wp_remote_post( $url, array(
                    'method' => 'POST',
                    'timeout' => 45,
                    'redirection' => 5,
                    'httpversion' => '1.0',
                    'blocking' => true,
                    'headers' => array(),
                    'body' => $post_data,//json_encode( array( 'id' => $id_str)),
                    'cookies' => array()
                    )
                );

                if( is_wp_error( $response ) ) {
                } else {

                }
            }

            update_post_meta($post->ID, "brt_twitter_username", $username);
            update_post_meta($post->ID, 'brt_last_checked', time());
        }
      }
    }
}

add_filter( 'manage_edit-twuser_columns', 'set_custom_edit_twuser_columns' );
add_action( 'manage_twuser_posts_custom_column' , 'brt_twitter_column', 10, 2 );

function set_custom_edit_twuser_columns($columns) {
    return $columns 
         + array('brt_username' => __('Username'), 'brt_tweets' => __('Tweeted urls'));
}

function brt_twitter_column( $column, $post_id ) {
    global $wpdb;
    switch ( $column ) {

      case 'brt_username':
        echo get_post_meta( $post_id , 'brt_twitter_username' , true ); 
        break;
      case 'brt_tweets':
        $tweeted_urls_table_name = $wpdb->prefix . "tweeted_urls";
        $numoftweets = $wpdb->get_col( $wpdb->prepare("SELECT count(1)  FROM $tweeted_urls_table_name WHERE `user` = %d", $post_id ));
        echo $numoftweets[0];
        echo "&nbsp; [<a href='admin.php?page=tweets_info&tuid=".$post_id."'>View tweets</a>]"; 
        break;
    }
}

add_action( 'quick_edit_custom_box', 'display_custom_quickedit_twuser', 10, 2 );

function display_custom_quickedit_twuser( $column_name, $post_type ) {
    if ($column_name != 'brt_username') return;
    static $printNonce = TRUE;
    if ( $printNonce ) {
        $printNonce = FALSE;
        wp_nonce_field( plugin_basename( __FILE__ ), 'twuser_edit_nonce' );
    }

    ?>
    <fieldset class="inline-edit-col-left inline-edit-twuser">
      <div class="inline-edit-col inline-edit-<?php echo $column_name ?>">
        <?php 
         switch ( $column_name ) {
         case 'brt_username':
             ?>
             <label>
                <span class="title">Twitter username</span>
                <span class="input-text-wrap"><input id="brt_twitter_username" name="brt_twitter_username" /></span>
            </label>
             <?php
             break;
         }
        ?>
      </div>
    </fieldset>
    <?php
}

// Add to our admin_init function
add_action('save_post', 'brt_save_quick_edit_data');
 
function brt_save_quick_edit_data($post_id) {
    // verify if this is an auto save routine. If it is our form has not been submitted, so we dont want
    // to do anything
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return $post_id;   

    $post = get_post($post_id);
    if (isset($_POST['brt_twitter_username']) && ($post->post_type != 'revision')) {
        $brt_twitter_username = esc_attr($_POST['brt_twitter_username']);
        if ($brt_twitter_username)
            update_post_meta( $post_id, 'brt_twitter_username', $brt_twitter_username);    
        else
            delete_post_meta( $post_id, 'brt_twitter_username');    
    }      
    return $post_id;
}

// Add to our admin_init function
add_action('admin_footer', 'brt_quick_edit_javascript');
 
function brt_quick_edit_javascript() {
    global $current_screen;
    if (($current_screen->id != 'edit-twuser') || ($current_screen->post_type != 'twuser')) return;
     
    ?>
    <script type="text/javascript">
    <!--
    function set_inline_twitter_username_set(brt_username, nonce) {
        // revert Quick Edit menu so that it refreshes properly
        console.log('?????');
        console.log('brt_username:'+brt_username);
        console.log('nonce:'+nonce);
        inlineEditPost.revert();
        var brt_twitter_username = document.getElementById('brt_twitter_username');
        var nonceInput = document.getElementById('brt_username_set_noncename');
//        nonceInput.value = nonce;

        brt_twitter_username.value =brt_username;

    }
    //-->
    </script>
    <?php
}

// Add to our admin_init function
add_filter('post_row_actions', 'brt_expand_quick_edit_link', 10, 2);
 
function brt_expand_quick_edit_link($actions, $post) {
    global $current_screen;
    if (($current_screen->id != 'edit-twuser') || ($current_screen->post_type != 'twuser')) return $actions;
 
    $nonce = wp_create_nonce( 'brt_username_set_noncename'.$post->ID);
    $brt_twitter_username = get_post_meta( $post->ID, 'brt_twitter_username', TRUE);
    $actions['inline hide-if-no-js'] = '<a href="#" class="editinline" title="';
    $actions['inline hide-if-no-js'] .= esc_attr( __( 'Edit this item inline' ) ) . '" ';
    $actions['inline hide-if-no-js'] .= " onclick=\"set_inline_twitter_username_set('{$brt_twitter_username}', '{$nonce}')\">";
    $actions['inline hide-if-no-js'] .= __( 'Quick&nbsp;Edit' );
    $actions['inline hide-if-no-js'] .= '</a>';
    return $actions;   
}

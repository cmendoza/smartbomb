<?php
/*
 * Add page to display latest tweets
 */
add_action('admin_menu', 'brt_register_recent_tweets_page');

function brt_register_recent_tweets_page() {
  add_submenu_page('edit.php?post_type=twuser', 'Recent Tweets', 'Recent Tweets', 'manage_options', 'recent_tweets', 'brt_recent_tweets');
}

function brt_recent_tweets() {
    include_once('tweets-table.php');
    wp_enqueue_script( 'update_stats', get_template_directory_uri() . '/javascripts/update-stats.js', array('jquery') );
    wp_enqueue_script( 'colorbox', get_template_directory_uri().'/javascripts/jquery.colorbox-min.js', array('jquery') );
    wp_enqueue_style('colorbox', get_template_directory_uri().'/stylesheets/colorbox.css');
    wp_enqueue_script( 'dygraph', get_template_directory_uri().'/javascripts/dygraph-combined.js', array('jquery') );
 ?>
 <script type="text/javascript">
     var loading_img = '<?php echo get_bloginfo('template_url'); ?>/images/ajax_loader.gif';
 </script>
    <div class="wrap">
        
        <div id="icon-users" class="icon32"><br/></div>
        <h2>Recent Tweets</h2>
<?php
    $RecentTweetsListTable = new Recent_Tweets_List_Table();

    //Fetch, prepare, sort, and filter our data...
    $RecentTweetsListTable->prepare_items();
?>
        <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
        <form id="tweets-filter" method="get">
            <!-- For plugins, we also need to ensure that the form posts back to our current page -->
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <!-- Now we can render the completed list table -->
            <?php $RecentTweetsListTable->display() ?>
        </form>
        <div style='display:none'>
            <div id='inline_content' style='padding:10px; background:#fff;'>
            <p>This data is retrieved from Bitly on each request.</p>
<!--            <figure style="width: 95%; height: 300px; margin: 0 auto;" id="myChart"></figure> -->
<div id="graph" style="height: 356px; margin: 0 auto; width: 90%;"></div>
            </div>
        </div>

<?php
?>

    </div>
<?php
}

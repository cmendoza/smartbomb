<?php
global $brt_db_version;
$brt_db_version = "1.0";

function brt_install_tables() {
	global $wpdb;
	global $brt_db_version;

	$table_name = $wpdb->prefix . "tweeted_urls";
      
//id, user, date, tweet, short-url, url, ...
	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {

	   $sql = "CREATE TABLE $table_name (
	  id mediumint(9) NOT NULL AUTO_INCREMENT,
	  user mediumint(9) NOT NULL,
	  tweet_id varchar(30) DEFAULT '' NOT NULL,
	  time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
	  tweet varchar(300) DEFAULT '' NOT NULL,
	  short_url varchar(100) DEFAULT '' NOT NULL,
	  url varchar(300) DEFAULT '' NOT NULL,
	  bitly int(1) DEFAULT '' NOT NULL,
	  sscore int(3) DEFAULT '' NOT NULL,
	  shared int(5) DEFAULT '' NOT NULL,
	  globalclicks int(5) DEFAULT '' NOT NULL,
	  retweets int(5) DEFAULT '' NOT NULL,
	  updated timestamp DEFAULT '' NOT NULL,
	  published int(5) DEFAULT '' NOT NULL,
	  UNIQUE KEY id (id)
	    );";

	   require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	   dbDelta($sql);
	}
 
 // id, tweet, date, clicks
	$table_name = $wpdb->prefix . "url_stats";
      
//id, user, date, tweet, short-url, url
    if($wpdb->get_var("show tables like '$table_name'") != $table_name) {

       $sql = "CREATE TABLE $table_name (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      tweet mediumint(9) NOT NULL,
      time varchar(12) NOT NULL,
      clicks varchar(10) DEFAULT '' NOT NULL,
      UNIQUE KEY id (id)
        );";

       require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
       dbDelta($sql);
    }
	add_option("brt_db_version", $brt_db_version);
}

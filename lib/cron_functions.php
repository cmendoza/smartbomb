<?php
define('bitlyAccessToken', '');
define('twConsumerKey', '');
define('twConsumerSecret', '');
define('twAccessToken', '');
define('twAccessSecret', '');

/** Setup cron schedule **/
add_filter('cron_schedules','brt_cron_add_schedules');

function brt_cron_add_schedules($schedules) {
    $schedules['2min'] = array('interval' => 60, 'display' => 'every minute');
    $schedules['5min'] = array('interval' => 300, 'display' => 'every 5 minutes');
    $schedules['30min'] = array('interval' => 1800, 'display' => 'every 30 minutes');
    return $schedules;
}

function brt_cron_settings() {
    if (!wp_next_scheduled('brt_cron_update_tweets')) {
        wp_schedule_event( time(), '5min', 'brt_cron_update_tweets' );
    }
    if (!wp_next_scheduled('brt_cron_update_tweet_stats')) {
        wp_schedule_event( time()+120, '5min', 'brt_cron_update_tweet_stats' );        
    }
}

/** Schedule tweets updates **/
add_action('brt_cron_update_tweets', 'brt_update_tweets');
/** Schedule tweet stats updates **/
add_action('brt_cron_update_tweet_stats', 'brt_update_stats');

function brt_update_tweets() {
    global $wpdb;
    $table_name = $wpdb->prefix . "tweeted_urls";

    date_default_timezone_set('America/Denver');

    require 'lib/tmhOAuth.php';
    require 'lib/tmhUtilities.php';
    include_once('lib/bitly.php');
    $tmhOAuth = new tmhOAuth(array(
      'consumer_key' => twConsumerKey,
      'consumer_secret' => twConsumerSecret,
      'user_token' => twAccessToken,
      'user_secret' => twAccessSecret,
    ));

    //Twitter users
    $args = array( 
        'post_type' => array('twuser'),
    'numberposts' => 5,
	'order' => 'ASC',
	'orderby' => 'meta_value',
	'meta_key' => 'brt_last_checked'
  //Previous settings
//        'numberposts' => 2,
//        'orderby' => 'rand',
        );
    $twitter_users = get_posts( $args );
    $usernames = array();
    foreach ($twitter_users as $username) {
        $user_name = get_post_meta( $username->ID, 'brt_twitter_username' , true );
        $last_id = get_post_meta( $username->ID, 'brt_last_tweet_id' , true );
        $usernames[] = array('id' => $username->ID, 'username' => $user_name, 'last_id' => $last_id);
    }

    $counter = 0;
    foreach ($usernames as $user) {
//    echo "<h2>".$user['username']." last saved id: ".$user['last_id']."</h2>\n";

        $last_saved_id = $user['last_id'];
        
        if ($last_saved_id) {
        	//request tweets since last id
//            echo "requesting from last saved id:$last_saved_id<br/>";
            $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/statuses/user_timeline'), array(
              'include_entities' => '1',
              'include_rts' => '1',
              'screen_name' => $user['username'],
              'count' => 100,
              'since_id' => $last_saved_id
            ));
        } else {
        	//get last 100 tweets
//            echo "requesting latest 100<br/>";
            $code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/statuses/user_timeline'), array(
              'include_entities' => '1',
              'include_rts' => '1',
              'screen_name' => $user['username'],
              'count' => 100,
            ));
        }

        if ($code == 200) {
            $timeline = json_decode($tmhOAuth->response['response'], true);
            foreach ($timeline as $tweet) {
                $urls = array();
                foreach ($tweet['entities'] as $type => $things) {
                    foreach ($things as $entity => $value) {
                        switch ($type) {
                            case 'urls':
                                $urls[] = empty($value['expanded_url']) ? $value['url'] : $value['expanded_url'];
                                break;
                        }
                    }
                }

                $entified_tweet = tmhUtilities::entify_with_options($tweet);
                $is_retweet = isset($tweet['retweeted_status']);


                foreach($urls as $url) {

                    $post_exists = $wpdb->get_row( $wpdb->prepare("SELECT * FROM $table_name WHERE tweet_id = '%s' and short_url = '%s';",$tweet['id_str'], $url));
                    $bitly_url = 0;
                    if ($post_exists == null ) {
                        date_default_timezone_set('America/Denver');

                        $long_url = bitly_v3_expand($url);
                        if (isset($long_url[0]['long_url']) && $url != $long_url[0]['long_url']) {
                            $bitly_url = 1;
                            $expanded_url = $long_url[0]['long_url'];
                        } else {
                            $expanded_url = "";
                        }
                        //only save if is a bitly URL
                        if ($bitly_url == 1) {
                            $wpdb->insert( 
                                $table_name, 
                                array( 
                                    'user' => $user['id'], 
                                    'tweet_id' => $tweet['id_str'], 
                                    'time' => date('Y-m-d H:i:s', strtotime($tweet['created_at'])), 
                                    'tweet' => $tweet['text'], 
                                    'short_url' => $url, 
                                    'url' => $expanded_url,
                                    'bitly' => $bitly_url
                                ), 
                                array( 
                                    '%d',
                                    '%s', 
                                    '%s', 
                                    '%s', 
                                    '%s',
                                    '%s',
                                    '%d' 
                                ) 
                            );
                        }
                        if ($bitly_url== 1 && $wpdb->insert_id) {
                            brt_update_stats_single($url, $wpdb->insert_id);
                        }
                        $counter++;
                    } else {
//                        echo "<br/>Skipping this post (already saved)";
                    }
                }

            }
        //update post meta for each user
        update_post_meta( $user['id'], 'brt_last_tweet_id', $tweet['id_str']); 
        update_post_meta($user['id'], 'brt_last_checked', time());
        } else {
          tmhUtilities::pr($tmhOAuth->response);
        }

    }
}

function brt_update_stats() {
    global $wpdb;
    $counter = 0;
    include_once('lib/bitly.php');

    $tweets_table_name = $wpdb->prefix . "tweeted_urls";
    $tweet_stats_table_name = $wpdb->prefix . "url_stats";

    $get_tweets = $wpdb->get_results("SELECT * FROM $tweets_table_name where time > '".date('Y-m-d G:i:s', strtotime('-7 days')) ."' and updated between '".date('Y-n-j G:i:s', time()-7200) ."' and '".date('Y-n-j G:i:s', time())."' or updated = '0000-00-00 00:00:00' ORDER BY RAND( ) limit 0,10");

    foreach ( $get_tweets as $tweet ) {
        $clicks_by_day = bitly_v3_clicks_by_day($tweet->short_url);
        if (isset($clicks_by_day[0]['clicks'])) {
            foreach ($clicks_by_day[0]['clicks'] as $clicks) {
	            $post_exists = $wpdb->get_var( "SELECT * FROM $tweet_stats_table_name WHERE tweet = '".$tweet->id."' and time = '".$clicks['day_start']."';" );
	            if (!$post_exists ) {
	                $stats = $wpdb->get_results("INSERT INTO $tweet_stats_table_name (`tweet`, `time`, `clicks`) VALUES ( '".$tweet->id."', ".$clicks['day_start']." , ".$clicks['clicks'].") ");
	            } else {
	                $stats = $wpdb->get_results("UPDATE $tweet_stats_table_name SET `clicks` = '".$clicks['clicks']."' WHERE `tweet` = ".$tweet->id." and time = '".$clicks['day_start']."';");
	            }
	            $counter++;
            }
        }

        $social_score = bitly_v3_link_social(bitlyAccessToken, $tweet->short_url);
        if (isset($social_score[$tweet->short_url])) {
            $sscore = $social_score[$tweet->short_url];
        } else {
            $sscore = 0;
        }
        if ($tweet->short_url == $tweet->url) {
            $long_url = bitly_v3_expand($tweet->short_url);
            if (isset($long_url[0]['long_url'])) {
                $url = $long_url[0]['long_url'];
                $update_query = "UPDATE $tweets_table_name SET `sscore` = '$sscore', url='$url', bitly=1, updated= NOW() WHERE `id` = $tweet->id;";
            } else {
                $update_query = "UPDATE $tweets_table_name SET `sscore` = '$sscore', bitly=0, updated= NOW() WHERE `id` = $tweet->id;";
            }
        } else {
            $update_query = "UPDATE $tweets_table_name SET `sscore` = '$sscore', updated= NOW() WHERE `id` = $tweet->id;";
        }
        $score = $wpdb->get_results($update_query);
    }
}
<?php
/*
 * Code to create, test and send the newsletter 
 * Adapted from WordChimp plugin by David Hudson (http://hudsoncs.com/)
 */

// Include some MailChimp API goodness
require_once 'MCAPI.class.php';

// Administrator
// Create navigation buttons
add_action('admin_menu', 'sb_newsletter_menu');
function sb_newsletter_menu() {
	$page_capability = get_option( 'sb_newsletter_page_capability' ) == '' ? 'manage_options' : get_option( 'sb_newsletter_page_capability' );
	$campaigns_capability = get_option( 'sb_newsletter_campaigns_capability' ) == '' ? 'manage_options' : get_option( 'sb_newsletter_campaigns_capability' );
	$settings_capability = get_option( 'sb_newsletter_settings_capability' ) == '' ? 'manage_options' : get_option( 'sb_newsletter_settings_capability' );

	$page = add_menu_page('Newsletter', 'Newsletter', $page_capability, 'newsletter', 'sb_newsletter_dashboard');
	$campaigns_page = add_submenu_page('newsletter', 'Campaign Stats', 'Campaign Stats', $campaigns_capability, 'newsletter-campaigns', 'sb_newsletter_campaigns_page');
	$settings_page = add_submenu_page('options-general.php', 'Newsletter Settings', 'Newsletter', $settings_capability, __FILE__, 'sb_newsletter_settings_page');
	
	add_action( 'admin_print_styles-' . $page, 'sb_newsletter_admin_styles' );
	add_action( 'admin_print_scripts-' . $page, 'sb_newsletter_admin_scripts' );
	
	add_action( 'admin_print_styles-' . $campaigns_page, 'sb_newsletter_admin_styles' );
	add_action( 'admin_print_scripts-' . $campaigns_page, 'sb_newsletter_admin_scripts');
	
	add_action( 'admin_print_styles-' . $settings_page, 'sb_newsletter_admin_styles' );
	add_action( 'admin_print_scripts-' . $settings_page, 'sb_newsletter_admin_scripts' );
	
	add_action( 'admin_init', 'sb_newsletter_settings' );
}

// Initialize admin page styles
add_action( 'admin_init', 'sb_newsletter_admin_init' );

// Setup ajax calls
add_action( 'wp_ajax_newsletter_get_post', 'sb_newsletter_get_post' );
add_action( 'wp_ajax_campaign_preview', 'sb_newsletter_campaign_preview' );


function sb_newsletter_admin_init() {
	wp_register_style( 'newsletterStyle', '/wp-content/plugins/wordchimp/style.css' );
	wp_register_script( 'newsletterjQuery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js' );
	wp_register_script( 'newsletterjQueryUI', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js' );
	wp_register_script( 'newsletterScript', '/wp-content/plugins/wordchimp/js/script.js' );
}

function sb_newsletter_admin_styles() {
   wp_enqueue_style( 'newsletterStyle' );
}

function sb_newsletter_admin_scripts() {
   wp_enqueue_script( 'newsletterjQuery' );
   wp_enqueue_script( 'newsletterjQueryUI' );
   wp_enqueue_script( 'newsletterScript' );
}

function sb_newsletter_settings() {
	register_setting( 'sb_newsletter_options_group', 'sb_newsletter_mailchimp_api_key' );
	register_setting( 'sb_newsletter_options_group', 'sb_newsletter_campaign_from_name' );
	register_setting( 'sb_newsletter_options_group', 'sb_newsletter_campaign_from_email' );
	register_setting( 'sb_newsletter_options_group', 'sb_newsletter_logo_url' );
	register_setting( 'sb_newsletter_options_group', 'sb_newsletter_timestamp_format' );
	register_setting( 'sb_newsletter_options_group', 'sb_newsletter_template' );
	register_setting( 'sb_newsletter_options_group', 'sb_newsletter_page_capability' );
	register_setting( 'sb_newsletter_options_group', 'sb_newsletter_campaigns_capability' );
	register_setting( 'sb_newsletter_options_group', 'sb_newsletter_settings_capability' );
}

function sb_newsletter_dashboard() {
	global $random;
	global $wpdb;
	
	echo <<<EOF
	<div class='wrap wordchimp'>
		<div id='icon-themes' class='icon32'></div>
		<h2 class="nav-tab-wrapper">
			<a href="admin.php?page=newsletter" class="nav-tab nav-tab-active">Dashboard</a>
			<a href="admin.php?page=newsletter-campaigns" class="nav-tab">Stats</a>
			<a href="options-general.php?page=wordchimp/newsletter.php" class="nav-tab">Settings</a>
		</h2>
EOF;

	$newsletter_step = filter_input(INPUT_POST, 'wp_cmd', FILTER_SANITIZE_STRING);
	switch ($newsletter_step) {
		default:
		case "step1":
			if (get_option( 'mailchimp_api_key' ) == "") {
				echo "<p class='wordchimp_error'>You must enter your MailChimp API key in the settings page before you can continue. " . $random['whoops'][rand(0, count($random['whoops'])-1)] . "</p>";
			} else {
				$api = new MCAPI_WordChimp(get_option( 'mailchimp_api_key') );
				$types = $api->templates(array('user' => true, 'gallery' => true));
				
				echo "<h1>Step 1: Select Template</h1><span class='wordchimp_notice'>Templates provided by you (shown below as user) and by the MailChimp gallery. Not all templates shown are 100% compatible with WordChimp. Try the 'Simple Newsletter' template under the gallery section if you're unsure.</span>";
				//print_r($templates);
				foreach ($types as $type => $templates) {
					if ($type == 'user') {
						echo "<div style='clear:both;overflow:auto;width:100%;'><h4>{$type}</h4>";
						foreach ($templates as $template) {
							echo "<div class='template_select_box' template_id='{$template['id']}'><span>{$template['name']}</span><img src='{$template['preview_image']}' /></div>";
						}
						echo "</div>";
					}
				}
				
				echo <<<EOF
				<form method='post' id='step1_form'>
					<input type='hidden' name='wp_cmd' value='step2' />
					<input type='hidden' name='template_id' />
				</form>
EOF;
			}
		break;
		
		case "step2":
			if (get_option( 'mailchimp_api_key' ) == "") {
				echo "<p class='wordchimp_error'>You must enter your MailChimp API key in the settings page before you can continue. " . $random['whoops'][rand(0, count($random['whoops'])-1)] . "</p>";
			} else {
				echo "
					<h1>Step 2: Select Posts</h1>
					<span class='wordchimp_notice'>
						<strong>Instructions:</strong>
						<ol style='width:50%'>
							<li>Under 'Select Posts', choose a template section you would like to insert a post into. If you're unsure, most templates have section names like 'main' where the main content would go. Feel free to experiment!</li>
							<li>Click the post you would like to insert.</li>
							<li>On the right, you should notice your post inserted into the desired section.</li>
							<li>When you're done adding all of your posts. click 'Next' at the very bottom.</li>
						</ol>
					</span>";

				// Pull selected template info
				$api = new MCAPI_WordChimp(get_option( 'mailchimp_api_key') );
				$template_info = $api->templateInfo($_POST['template_id']);
				
				// Get last 40 posts
				$sql = "SELECT id, post_author, post_date, post_content, post_title, post_excerpt, post_name FROM {$wpdb->prefix}posts WHERE post_type = 'post' AND post_status = 'publish' ORDER BY post_date DESC LIMIT 40";
				$posts = $wpdb->get_results($sql, ARRAY_A);
				
				echo "
				<div style='width:20%;float:left;' id='posts_listing'>
					<h3>Select Posts</h3>
					<select id='wordchimp_section_select'>
						<option value='' disabled>Select Template Section</option>
					";
				
				foreach ($template_info['sections'] as $section) {
					echo "<option>{$section}</option>";
				}
				
				echo "
					</select>
					<ul style='margin:15px 0px;'>
						";
						
				foreach ($posts as $post) {
					echo "<li id='post_{$post['id']}' post_id='{$post['id']}'>{$post['post_title']}</li>";
				}
				
				echo <<<EOF
					</ul>
				</div>
				<div style='width:75%;float:right;' id='posts_used'>
					<h3>Template Sections</h3>
					<form method='post' id='step2_form'>
						<input type='hidden' name='wp_cmd' value='step3' />
						<input type='hidden' name='template_id' value='{$_POST['template_id']}' />
EOF;
	
				// Setup sections
				foreach ($template_info['default_content'] as $section => $content) {
					if ($section == 'braintrust_avatars') {
						$avatars = brt_wordchimp_get_avatars();
						echo "<h3>{$section}</h3><textarea name='html_{$section}' id='html_{$section}'>" . str_replace("\t", '', str_replace("   ",'',htmlspecialchars($avatars))) . "</textarea>"; // Show avatars in text area
					} else {
					echo "<h3>{$section}</h3><textarea name='html_{$section}' id='html_{$section}'>" . str_replace("\t", '', str_replace("   ",'',htmlspecialchars($content))) . "</textarea>"; // Show text area. Remove all the extra tabs and spacing that usually shows up in these templates.
					}
				}

				echo <<<EOF
					</form>
				</div>
				<div style='clear:both;'>
					<input type="submit" class="button-primary" value="Next" id="step2_submit" />
				</div>
EOF;
			}
		break;
		
		case "step3":
			if (get_option( 'mailchimp_api_key' ) == "") {
				echo "<p class='wordchimp_error'>You must enter your MailChimp API key in the settings page before you can continue. " . $random['whoops'][rand(0, count($random['whoops'])-1)] . "</p>";
			} else {
				$api = new MCAPI_WordChimp(get_option( 'mailchimp_api_key') );

				$retval = $api->lists();

				if ($api->errorCode){
					echo "<p class='wordchimp_error'>Something went wrong when trying to get your MailChimp e-mail lists.  " . $random['whoops'][rand(0, count($random['whoops'])-1)] . " {$api->errorCode} {$api->errorMessage}</p>";
				} else {
					echo "<h1>Step 3: Select List</h1><span class='wordchimp_notice'>Please select the MailChimp list you would like to send to.</span><br /><br />";
					echo "
						<form method='post'>
							<input type='hidden' name='wp_cmd' value='step4' />
							<input type='hidden' name='template_id' value='{$_POST['template_id']}' />
							";
					
					// Add the previously sent posts
					foreach ($_POST as $key => $value) {
						if ($key != 'wp_cmd' && $key != 'template_id') {
							echo "<input type='hidden' name='{$key}' value=\"" . htmlspecialchars(stripslashes($value)) . "\" />";
						}
					}

					// Show MailChimp lists for selection
					foreach ($retval['data'] as $count => $list) {
						if ($count == 0) {
							echo "<label><input type='radio' name='mailchimp_list_id' value='{$list['id']}' CHECKED /> {$list['name']} ({$list['stats']['member_count']} Subscribers)</label><br />";
						} else {
							echo "<label><input type='radio' name='mailchimp_list_id' value='{$list['id']}' /> {$list['name']} ({$list['stats']['member_count']} Subscribers)</label><br />";
						}
					}
					echo <<<EOF
					<p class="submit">
						<input type="submit" class="button-primary" value="Next" />
					</p>
				</form>
EOF;
				}
			}
		break;
		
		case "step4":
			if (get_option( 'mailchimp_api_key' ) == "") {
				echo "<p class='wordchimp_error'>You must enter your MailChimp API key in the settings page before you can continue. " . $random['whoops'][rand(0, count($random['whoops'])-1)] . "</p>";
			} else {
				echo "
					<h1>Step 4: Complete Campaign Information</h1>
					<form method='post'>
						<input type='hidden' name='wp_cmd' value='step5' />
						<input type='hidden' name='template_id' value='{$_POST['template_id']}' />
						<input type='hidden' name='mailchimp_list_id' value='{$_POST['mailchimp_list_id']}' />
						";
						
				// Add modified template sections
				foreach ($_POST as $key => $value) {
					if ($key != 'wp_cmd' && $key != 'template_id' && $key != 'mailchimp_list_id') {
						echo "<input type='hidden' name='{$key}' value=\"" . htmlspecialchars(stripslashes($value)) . "\" />";
					}
				}
				
				echo "<span class='wordchimp_notice'>Please enter the campaign information.</span><br /><br />";
				
				// Setup campaign options
				$wordchimp_campaign_from_email = htmlspecialchars(get_option( 'wordchimp_campaign_from_email' ));
				$wordchimp_campaign_from_name = htmlspecialchars(get_option( 'wordchimp_campaign_from_name' ));
				echo "
				<table class='wordchimp_form_table'>
					<tr valign='top'>
						<th scope='row'>Title</th>
						<td><input type='text' name='wordchimp_campaign_title' /></td>
					</tr>
					<tr valign='top'>
						<th scope='row'>Subject</th>
						<td><input type='text' name='wordchimp_campaign_subject' /></td>
					</tr>
					<tr valign='top'>
						<th scope='row'>From Email</th>
						<td><input type='text' name='wordchimp_campaign_from_email' value='{$wordchimp_campaign_from_email}' /></td>
					</tr>
					<tr valign='top'>
						<th scope='row'>From Name</th>
						<td><input type='text' name='wordchimp_campaign_from_name' value='{$wordchimp_campaign_from_name}' /></td>
					</tr>
				</table>
				<h3>Campaign Tracking</h3>
				<label><input type='checkbox' name='wordchimp_campaign_track_opens' value='true' checked /> Track number of times email was opened</label><br />
				<label><input type='checkbox' name='wordchimp_campaign_track_html_clicks' value='true' checked /> Track number of times a user clicked an HTML link</label><br />
				<label><input type='checkbox' name='wordchimp_campaign_track_text_clicks' value='true' checked /> Track number of times a user clicked a text link</label>";
				
				echo <<<EOF
					<p class="submit">
						<input type="submit" class="button-primary" value="Next" />
					</p>
				</form>
EOF;
			}
		break;
		
		case "step5":
			if (get_option( 'mailchimp_api_key' ) == "") {
				echo "<p class='wordchimp_error'>You must enter your MailChimp API key in the settings page before you can continue. " . $random['whoops'][rand(0, count($random['whoops'])-1)] . "</p>";
			} else {
				// Build the campaign
				$api = new MCAPI_WordChimp(get_option( 'mailchimp_api_key' ));

				$type = 'regular';

				$opts['template_id'] 	= $_POST['template_id'];
				$opts['list_id'] 			= $_POST['mailchimp_list_id'];
				$opts['subject'] 			= $_POST['wordchimp_campaign_subject'];
				$opts['from_email'] 	= $_POST['wordchimp_campaign_from_email'];
				$opts['from_name'] 	= $_POST['wordchimp_campaign_from_name'];
				$opts['tracking'] 		= array('opens' => $_POST['wordchimp_campaign_track_opens'] == 'true' ? true : false, 'html_clicks' => $_POST['wordchimp_campaign_track_html_clicks'] == 'true' ? true : false, 'text_clicks' => $_POST['wordchimp_campaign_track_text_clicks'] == 'true' ? true : false);
				$opts['authenticate'] 	= true;
				$opts['title'] 				= $_POST['wordchimp_campaign_title'];
				
				foreach ($_POST as $key => $value) {
					if ($key != 'wp_cmd' && $key != 'template_id' && $key != 'mailchimp_list_id') {
						$content[$key] = stripslashes($value);
					}
				}

				$campaignId = $api->campaignCreate($type, $opts, $content);
				
				if ($api->errorCode){
					echo "<p class='wordchimp_error'>There was an error creating your campaign. " . $random['whoops'][rand(0, count($random['whoops'])-1)] . " {$api->errorCode} {$api->errorMessage}</p>";
				} else {
					echo "<h1>Step 5: Test/Send</h1><p class='wordchimp_success'>Great success! Your campaign was created. {$random['compliment'][rand(0, count($random['compliment'])-1)]} Now what?</p>";
					
					echo "<a href='" . get_bloginfo('wpurl') . "/wp-admin/admin-ajax.php?action=wordchimp_campaign_preview&cid={$campaignId}' target='_blank'>Preview Campaign in Browser</a><br /><br />";
					echo <<<EOF
					<h3>Send a test?</h3>
					<form method='post'>
						<input type='hidden' name='wp_cmd' value='step6' />
						<input type='hidden' name='template_id' value='{$_POST['template_id']}' />
						<input type='hidden' name='mailchimp_campaign_id' value='{$campaignId}' />
						<input type='text' name='mailchimp_test_emails' value='test@email.com, test@otheremail.com' onClick="this.value='';" />
EOF;
						// Add modified template sections
						foreach ($_POST as $key => $value) {
							if ($key != 'wp_cmd' && $key != 'template_id' && $key != 'mailchimp_list_id') {
								echo "<input type='hidden' name='{$key}' value=\"" . htmlspecialchars(stripslashes($value)) . "\" />";
							}
						}
						
						echo <<<EOF
						<p class="submit">
							<input type="submit" class="button-primary" value="Send Test" />
						</p>
					</form>
					<h3>Send fo' real?</h3>
					<form method='post'>
						<input type='hidden' name='wp_cmd' value='step7' />
						<input type='hidden' name='mailchimp_campaign_id' value='{$campaignId}' />
						<p class="submit">
							<input type="submit" class="button-primary" value="Send Fo' Real!" />
						</p>
					</form>
EOF;
				}
			}
		break;
		
		case "step6":
			$api = new MCAPI_WordChimp(get_option( 'mailchimp_api_key' ));
			$emails = explode(",", $_POST['mailchimp_test_emails']);
			$campaignId = $_POST['mailchimp_campaign_id'];
			
			$retval = $api->campaignSendTest($campaignId, $emails);
			
			if ($api->errorCode){
				echo "<p class='wordchimp_error'>Unable to send test campaign.  " . $random['whoops'][rand(0, count($random['whoops'])-1)] . " {$api->errorCode} {$api->errorMessage}</p>";
			} else {
				echo "<h1>Step 5: Test/Send</h1><span class='wordchimp_notice'>Great success! A test has been sent to the e-mail addresses you provided. " . $random['compliment'][rand(0, count($random['compliment']) -1)] . "</span><br /><br />";
			}
			
			echo "<a href='" . get_bloginfo('wpurl') . "/wp-admin/admin-ajax.php?action=wordchimp_campaign_preview&cid={$campaignId}' target='_blank'>Preview Campaign in Browser</a><br /><br />";
			echo <<<EOF
			<h3>Send a test?</h3>
			<form method='post'>
				<input type='hidden' name='wp_cmd' value='step6' />
				<input type='hidden' name='mailchimp_campaign_id' value='{$campaignId}' />
				<input type='text' name='mailchimp_test_emails' value='test@email.com, test@otheremail.com' onClick="this.value='';" />
EOF;
						// Add modified template sections
						foreach ($_POST as $key => $value) {
							if ($key != 'wp_cmd' && $key != 'template_id' && $key != 'mailchimp_list_id') {
								echo "<input type='hidden' name='{$key}' value=\"" . htmlspecialchars(stripslashes($value)) . "\" />";
							}
						}
						
						echo <<<EOF
				<p class="submit">
					<input type="submit" class="button-primary" value="Send Test" />
				</p>
			</form>
			<h3>Send fo' real?</h3>
			<form method='post'>
				<input type='hidden' name='wp_cmd' value='step7' />
				<input type='hidden' name='mailchimp_campaign_id' value='{$campaignId}' />
				<p class="submit">
					<input type="submit" class="button-primary" value="Send Fo' Real!" />
				</p>
			</form>
EOF;
		break;
		
		case "step7":
			$api = new MCAPI_WordChimp(get_option( 'mailchimp_api_key' ));
			$campaignId = $_POST['mailchimp_campaign_id'];
			$emails = explode(",", $_POST['mailchimp_test_emails']);
			
			$retval = $api->campaignSendNow($campaignId);
			
			if ($api->errorCode){
				echo "<p class='wordchimp_error'>Unable to send out campaign.  " . $random['whoops'][rand(0, count($random['whoops'])-1)] . " {$api->errorCode} {$api->errorMessage}</p>";
			} else {
				echo "<h1>Ding! Campaign Sent</h1><p class='wordchimp_success'>Great success! You're campaign has been sent out. " . $random['compliment'][rand(0, count($random['compliment']) -1)] . "</p><br /><br />";
			}
		break;
	}
	
	echo <<<EOF
	</div>
EOF;
}

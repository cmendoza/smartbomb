jQuery(document).ready(function($) {
	// Step 1
	jQuery(".template_select_box").on('click', function() {
		jQuery("#step1_form input[name=template_id]").val(jQuery(this).attr('template_id'));
		jQuery("#step1_form").submit();
	});
	
	// Step 2
	jQuery( "#newsletter_accordion" ).accordion({
            heightStyle: "content"
    });
    jQuery( "#newslettercontent" ).sortable({
	  placeholder: "newsletter-content-highlight"
	});
	jQuery( "#newslettercontent" ).disableSelection();

	jQuery('#templateinstructions').on('click', function() {
		var instructionslist = jQuery('#templateinstructionslist');
		var arrow = jQuery('#templateinstructions span');
		if(instructionslist.is(':visible')) {
    		arrow.addClass('ui-icon-triangle-1-s');
		} 
		if(!instructionslist.is(':visible')) {
    		arrow.removeClass('ui-icon-triangle-1-s');
		}
		instructionslist.slideToggle();
	});

	//add buttons to remove item
	jQuery('#newslettercontent').on('mouseenter', '.section', function() {
		var close = jQuery('<span></span>').addClass('removeitem').html('[ Delete this item ]');
		jQuery(this).find('.default_image').prepend(close);
	});
	jQuery('#newslettercontent').on('mouseleave', '.section', function() {
		jQuery(this).find('.removeitem').remove();
	});
	jQuery('#newslettercontent').on('click', '.removeitem', function() {
		var parentelem = jQuery(this).closest('.section');
		jQuery(parentelem).remove();
	});

	jQuery("#step2_submit").click(function() {
		var newslettercontent = jQuery('#newslettercontent').html();
		jQuery('#html_posts_content').val(newslettercontent);
		jQuery("#step2_form").submit();
	});
	

	jQuery('#posts_available li').click(function() {
		var selectedPost = jQuery(this);
		var post_id = selectedPost.attr('post_id');
		jQuery('#post_'+post_id).addClass('loading');
		jQuery.post(
			ajaxurl,
			{ action : 'newsletter_get_post', post_id : jQuery(selectedPost).attr('post_id') },
			function(post) {
//				jQuery('textarea#html_' + jQuery('#wordchimp_section_select').val()).val(jQuery('textarea#html_' + jQuery('#wordchimp_section_select').val()).val() + post);
				jQuery('#newslettercontent').append(post);
				jQuery('#post_'+post_id).removeClass('loading');
			}
		);
	});
});
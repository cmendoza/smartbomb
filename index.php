<?php 
wp_enqueue_script('masonry', get_bloginfo('template_url'). '/javascripts/jquery.masonry.min.js', array('jquery'), '', true );
get_header(); 
?>
			
			<div id="content" class="row">
			
				<div id="main" class="twelve columns clearfix" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('four columns tstory'); ?> role="article">
						
<?php
$tweet_id = get_post_meta($post->ID, 'brt_twitter_id_post', true);
$tweet_id_number = get_post_meta($post->ID, 'brt_twitter_id', true);
$tweet = get_post_meta($post->ID, 'brt_twitter_tweet', true);
$short_url = get_post_meta($post->ID, 'brt_twitter_short_url', true);
$url = get_post_meta($post->ID, 'brt_twitter_url', true);
$tweet_source = get_post_meta($post->ID, 'brt_tweet_source',true);
$source = get_post_meta($post->ID, 'brt_tweet_source', true);
if ($tweet_id) {
	$tweets_table_name = $wpdb->prefix . "tweeted_urls";

	$tweet = $wpdb->get_row("SELECT * from $tweets_table_name where id = $tweet_id");

}
if ($tweet) {
	$link = $tweet->short_url;
	$target = "_blank";
} else {
	$link = get_permalink();
	$target = "_self";
}
?>
						<?php
							if (has_post_thumbnail()) {
						?>
						<header>
							<a href="<?php echo $link; ?>" title="<?php the_title_attribute(); ?>" target="<?php echo $target; ?>"><?php the_post_thumbnail( 'wpf-featured' ); ?></a>
						<?php
							} else {
						?>
						<header class="default_image clearfix">
							<a href="<?php echo $link; ?>" title="<?php the_title_attribute(); ?>" target="<?php echo $target; ?>"><img src="<?php bloginfo('template_url'); ?>/images/default.jpg"/></a>
						<?php
							}
						?>
							
							<h2><a href="<?php echo $link; ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" target="<?php echo $target; ?>"><?php the_title(); ?></a></h2>
							
							<p class="meta"><a href="<?php echo $link; ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" target="<?php echo $target; ?>"><?php echo $tweet_source; ?>.</a></p>
						
						</header> <!-- end article header -->
					
						<section class="post_content clearfix">
							<?php the_content('Read more &raquo;'); ?>
					
						</section> <!-- end article section -->
						<footer class="twelve columns">
<?php
if ($tweet) {
//	$user_name = get_the_title($tweet->user);
//	$screen_name = get_post_meta($tweet->user, 'brt_twitter_username', true);
//	$avatar = twitter_avatar($screen_name );
//	$tooltip = "$tweet->globalclicks Global clicks<br/>$tweet->localclicks Brain trust clicks<br/>$tweet->shared Shares<br/>$tweet->retweets RTs";
	$tooltip = "$tweet->globalclicks Global clicks<br/>$tweet->localclicks Brain trust clicks<br/>$tweet->shared Shares<br/>$tweet->retweets RTs";
	$tweet_id = $tweet->tweet_id;
	$impact_back = false;

} else {
	$impact = get_post_meta($post->ID, 'brt_impact_factor', true);
	$tooltip = "Impact factor: ".$impact;
	$impact_back = true;
	$tweet_id = get_post_meta($post->ID, 'brt_twitter_id_post', true);
}
$user_name = get_post_meta($post->ID, 'brt_twitter_user_name', true);
$screen_name = get_post_meta($post->ID, 'brt_twitter_username', true);
$avatar = twitter_avatar($screen_name );
$impact = get_post_meta($post->ID, 'brt_impact_factor', true);
if ($impact) {
	$impact_back = true;
}
?>							
<div class="tinfo six columns">
	<div class="four columns">
		<a href="https://twitter.com/<?php echo $screen_name?>/status/<?php echo $tweet_id; ?>" target="_blank"><img style="padding:0 10px 0 0;" src="<?php echo $avatar?>" width='48' height='48' alt="<?php echo $user_name;?>"/></a>
	</div>
<?php if ($impact_back) { ?>
	<div class="four columns impact">
	<span>
		<?php echo $impact; ?>
	</span>
	</div>
<?php } ?>
	<div class="four columns kinetics">
	</div>
</div>
<div class="share six columns">
	<?php if(function_exists('wp_email')) { email_link(); } ?>
	<a href="https://twitter.com/intent/tweet?url=<?php echo $link; ?>" class="four columns" target="<?php echo $target; ?>">Tweet</a>
	<a href="https://plus.google.com/share?url=<?php echo $link; ?>" class="four columns" target="<?php echo $target; ?>">G+</a>
</div>
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
					
					<?php comments_template(); ?>
					
					<?php endwhile; ?>	
					
					<?php if (function_exists('page_navi')) { // if expirimental feature is active ?>
						
						<?php page_navi(); // use the page navi function ?>
						
					<?php } else { // if it is disabled, display regular wp prev & next links ?>
						<nav class="wp-prev-next">
							<ul class="clearfix">
								<li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
								<li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
							</ul>
						</nav>
					<?php } ?>		
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1>Not Found</h1>
					    </header>
					    <section class="post_content">
					    	<p>Sorry, but the requested resource was not found on this site.</p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->
    
    
			</div> <!-- end #content -->
			<div id="people" class="row">
				<h3>Follow The Smart People Who Shared These Stories</h3>
				<ul id="avatars" class="block-grid five-up mobile-two-up ten columns">
<?php
	$old_query = $wp_query;
	$args = array( 	
		'paged'		=> $paged,
		'post_type' => 'twuser',
		'posts_per_page' => 10,
		'orderby'	=> 'rand'
		);
	$wp_query = new WP_Query($args); 
	$featured_thumb_args = array(
			'class'	=> "clearfix",
		);
	if (have_posts()) : while (have_posts()) : the_post(); 
		echo braintrust_avatar($post->ID);
	endwhile;
	endif;
?>
				</ul>
<div id="all" class="two columns">
	<p class="nine columns"><a href="<?php bloginfo('url'); ?>/braintrust/#braintrust">See the whole braintrust</a></p>
</div>
			</div>
<?php get_footer(); ?>
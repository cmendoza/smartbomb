			<footer role="contentinfo">
			
					<div class="twelve columns">

						<div class="row">

							<nav class="ten columns clearfix">
								<p>&copy; <?php echo date('Y'); ?> SMARTBOMB INC. 962 Carolina St. San Francisco, CA 94107 USA</p>
								<?php bones_footer_links(); ?>
							</nav>

							<p class="attribution two columns"></p>

						</div>

					</div>
					
			</footer> <!-- end footer -->
		
		</div> <!-- end #container -->
		
		<!--[if lt IE 7 ]>
  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->
		
		<?php wp_footer(); // js scripts are inserted using this function ?>
<script type="text/javascript">
jQuery(window).load(function($) {
    var columns    = 3,
        setColumns = function() { columns = jQuery( window ).width() > 915 ? 3 : jQuery( window ).width() > 753 ? 2 : 1; 
//						        	console.log('columns:'+columns)
						        };
	 
    setColumns();
    jQuery( window ).resize( setColumns );
 
  	var $container = jQuery('#main'), 
  	  	$body = jQuery('body'),
      	colW = 330,
 //     	columns = null;
		initcolumns = 3;
  jQuery('#main').masonry({
    itemSelector : 'article',
	  resizable: false,
//	  columnWidth : $container.width() / initcolumns
	  columnWidth : function( containerWidth ) { return containerWidth / columns; }
  });

});
</script>
	</body>

</html>
<?php

// Include MailChimp API
require_once 'MCAPI.class.php';

class SB_Newsletter {

	var $page = '';
	var $random = array('compliment' => array(
										"Aren't you special?",
										"Way to go champ!",
										"MailChimp loves you.",
										"You're like a MailChimp/Wordpress ninja now.",
										"Did you just get your hair did? Nice!",
										"You're a bi-winner!",
										"Extremely groovy!",
										"You're making the whole office look good.",
										"You should run for President!"
										),
						'whoops' => array(
										"Whoops!",
										"What did you do?",
										"It's broke-a-did.",
										"Oh snap!",
										"Oh geeze...",
										"WWWWHHHHHHHYYYYYYYY?????"
										)
					);

    function SB_Newsletter() {
		// Create navigation buttons
        add_action('admin_menu', array( &$this, 'newsletter_menu') );
        // Initialize admin page styles
        add_action('admin_init', array( &$this, 'newsletter_admin_init') );
		
		add_action( 'wp_ajax_newsletter_campaign_preview', array( &$this, 'newsletter_campaign_preview') );

		add_filter( 'plugin_action_links', array( &$this, 'sbnewsletter_plugin_action_links'), 10, 2 );

    }

	function newsletter_admin_init() {
		wp_register_style( 'sbNewsletterStyle', get_stylesheet_directory_uri().'/admin/css/newsletter.css' );
		wp_register_style( 'sbNewsletterStyleA', get_stylesheet_directory_uri().'/admin/css/jquery-ui-1.10.3.custom.css' );
//		wp_register_script( 'sbNewsletterjQuery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js' );
//		wp_register_script( 'sbNewsletterjQueryUI', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js' );
		wp_register_script( 'sbNewsletterScript', get_stylesheet_directory_uri().'/admin/js/newsletter.js', array('jquery', 'jquery-ui-accordion', 'jquery-ui-sortable'), '', true );
	}

	function register_newsletter_settings() {
		register_setting( 'sbnewsletter_options_group', 'mailchimp_api_key' );
		register_setting( 'sbnewsletter_options_group', 'newsletter_campaign_from_name' );
		register_setting( 'sbnewsletter_options_group', 'newsletter_campaign_from_email' );
		register_setting( 'sbnewsletter_options_group', 'newsletter_logo_url' );
	//	register_setting( 'sbnewsletter_options_group', 'newsletter_strip_images' );
	//	register_setting( 'sbnewsletter_options_group', 'newsletter_show_author' );
	//	register_setting( 'sbnewsletter_options_group', 'newsletter_show_timestamp' );
		register_setting( 'sbnewsletter_options_group', 'newsletter_timestamp_format' );
		register_setting( 'sbnewsletter_options_group', 'newsletter_template' );
	//	register_setting( 'sbnewsletter_options_group', 'newsletter_use_excerpt' );
		register_setting( 'sbnewsletter_options_group', 'newsletter_page_capability' );
		register_setting( 'sbnewsletter_options_group', 'newsletter_campaigns_capability' );
		register_setting( 'sbnewsletter_options_group', 'newsletter_settings_capability' );
	}

	function newsletter_admin_styles() {
		wp_enqueue_style( 'sbNewsletterStyle' );
		wp_enqueue_style( 'sbNewsletterStyleA' );
	}

	function newsletter_admin_scripts() {
		wp_enqueue_script( 'sbNewsletterjQuery' );
		wp_enqueue_script( 'sbNewsletterjQueryUI' );
		wp_enqueue_script( 'sbNewsletterScript' );
	}

	function newsletter_menu() {
		$page_capability      = get_option( 'sbnewsletter_page_capability' ) == '' ? 'manage_options' : get_option( 'sbnewsletter_page_capability' );
		$campaigns_capability = get_option( 'sbnewsletter_campaigns_capability' ) == '' ? 'manage_options' : get_option( 'sbnewsletter_campaigns_capability' );
		$settings_capability  = get_option( 'sbnewsletter_settings_capability' ) == '' ? 'manage_options' : get_option( 'sbnewsletter_settings_capability' );

		$this->page           = add_menu_page( 'SbNewsletter', 'Newsletter', $page_capability, 'sbnewsletter', array( &$this, 'sbnewsletter_dashboard') );
		$campaigns_page = add_submenu_page( 'sbnewsletter', 'Campaign Stats', 'Campaign Stats', $campaigns_capability, 'sbnewsletter-campaigns', array( &$this, 'sbnewsletter_campaigns_page') );
		$settings_page  = add_submenu_page( 'options-general.php', 'Newsletter Settings', 'Newsletter', $settings_capability, 'sbnewsletter/settings', array( &$this, 'sbnewsletter_settings_page') );

		add_action( 'admin_print_styles-' . $this->page, array( &$this, 'newsletter_admin_styles') );
		add_action( 'admin_print_scripts-' . $this->page, array( &$this, 'newsletter_admin_scripts') );

		add_action( 'admin_print_styles-' . $campaigns_page, array( &$this, 'newsletter_admin_styles') );
		add_action( 'admin_print_scripts-' . $campaigns_page, array( &$this, 'newsletter_admin_scripts') );

		add_action( 'admin_print_styles-' . $settings_page, array( &$this, 'newsletter_admin_styles') );
		add_action( 'admin_print_scripts-' . $settings_page, array( &$this, 'newsletter_admin_scripts') );

		add_action( 'admin_init', array( &$this, 'register_newsletter_settings') );

	}


	function sbnewsletter_plugin_action_links( $links, $file ) {
		static $this_plugin;

		if ( ! $this_plugin ) {
			$this_plugin = plugin_basename( __FILE__ );
		}

		if ( $file == $this_plugin ) {
			$settings_link = '<a href="' . get_bloginfo( 'wpurl' ) . '/wp-admin/admin.php?page=sbnewsletter/settings">Settings</a>';
			array_unshift( $links, $settings_link );
		}

		return $links;
	}
	
	function sbnewsletter_dashboard() {
		global $wpdb;
		echo <<<EOF
		<div class='wrap sbnewsletter'>
			<div id='icon-themes' class='icon32'></div>
			<h2 class="nav-tab-wrapper">
				<a href="admin.php?page=sbnewsletter" class="nav-tab nav-tab-active">Dashboard</a>
				<a href="admin.php?page=sbnewsletter-campaigns" class="nav-tab">Stats</a>
				<a href="options-general.php?page=sbnewsletter/settings" class="nav-tab">Settings</a>
			</h2>
EOF;

		$wp_cmd = filter_input( INPUT_POST, 'wp_cmd', FILTER_SANITIZE_STRING );
		switch ( $wp_cmd ) {
			default:
			case "step1":
				if ( get_option( 'mailchimp_api_key' ) == "" ) {
					echo "<p class='newsletter_error'>You must enter your MailChimp API key in the settings page before you can continue. " . $this->random['whoops'][rand( 0, count( $this->random['whoops'] ) - 1 )] . "</p>";
				}
				else {
					$api   = new MCAPI_WordChimp( get_option( 'mailchimp_api_key' ) );
					$types = $api->templates( array( 'user' => true, 'gallery' => true ) );

					echo "<h1>Step 1: Select Template</h1><span class='newsletter_notice'>Templates provided by you (shown below as user) and by the MailChimp gallery. <br/>Not all templates shown are 100% compatible, in order to work the template <strong>must</strong> have a <em>posts_content</em> section.</span>";
					//print_r($templates);
					foreach ( $types as $type => $templates ) {
						if ( $type == 'user' ) {
							echo "<div style='clear:both;overflow:auto;width:100%;'><h4>{$type}</h4>";
							foreach ( $templates as $template ) {
								echo "<div class='template_select_box' template_id='{$template['id']}'><span>{$template['name']}</span><img src='{$template['preview_image']}' /></div>";
							}
							echo "</div>";
						}
					}

					echo <<<EOF
					<form method='post' id='step1_form'>
						<input type='hidden' name='wp_cmd' value='step2' />
						<input type='hidden' name='template_id' />
					</form>
EOF;
				}
				break;

			case "step2":
				if ( get_option( 'mailchimp_api_key' ) == "" ) {
					echo "<p class='newsletter_error'>You must enter your MailChimp API key in the settings page before you can continue. " . $this->random['whoops'][rand( 0, count( $this->random['whoops'] ) - 1 )] . "</p>";
				}
				else {

					// Pull selected template info
					$api           = new MCAPI_WordChimp( get_option( 'mailchimp_api_key' ) );
					$template_info = $api->templateInfo( $_POST['template_id'] );

					echo <<<EOF
						<h1>Step 2: Select Posts</h1>
						 <div id="newsletter_accordion">
						<h3>Newsletter</h3>
						<div id='content'>
						<span class='newsletter_notice'>
							<h3 id='templateinstructions' class='instructions'>Instructions <span class='ui-icon ui-icon-triangle-1-s'></span></h3>
							<ol id='templateinstructionslist'>
								<li>The last 20 posts are listed on the left side.</li>
								<li>Click the post you would like to add to the newsletter, it will be loaded on the newsletter template mockup.</li>
								<li>You can reorder the posts by dragging a post to the position you want it to appear on the newsletter.</li>
								<li>To delete a post from the newsletter just click on the 'Delete this item' link</li>
								<li>If you want to change something on the rest of the template the sections are in 'Template sections'.</li>
								<li>When you're done adding all of your posts. click 'Next' at the very bottom.</li>
							</ol>
						</span>
EOF;
					// Get last 20 posts
					$sql   = "SELECT id, post_author, post_date, post_content, post_title, post_excerpt, post_name FROM {$wpdb->prefix}posts WHERE post_type = 'post' AND post_status = 'publish' ORDER BY post_date DESC LIMIT 20";
					$posts = $wpdb->get_results( $sql, ARRAY_A );
					echo "<h3>Select Posts</h3><ul id='posts_available'>";
					foreach ( $posts as $post ) {
						echo "<li id='post_{$post['id']}' post_id='{$post['id']}'>{$post['post_title']}</li>";
					}

echo <<<EOF
						</ul>
						<div id='contentorder'>
						<div class='templatelogo'></div>
						<div class='templateheader'><h3>Today's Big Ideas</h3> ( This is only a mockup of the newsletter, it's not the final look of it.)</div>
						<div id='newslettercontent'></div>
						<div class='templatefooter'><h4>Footer</h4></div>
						</div>
						</div>
						<h3>Template Sections</h3>
						<div>
						<span class='newsletter_notice'>
							<strong>Warning:</strong>
							<ul style='width:50%'>
								<li>The textareas contain the different sections of the template.</li>
								<li>You can modify the text inside to add/change the section.</li>
								<li>Changes to the template can break the layout, it's recommended to edit the template directly in Mailchimp.</li>
							</ul>
						</span>
						<form method='post' id='step2_form'>
							<input type='hidden' name='wp_cmd' value='step3' />
							<input type='hidden' name='template_id' value='{$_POST['template_id']}' />
EOF;
					foreach ( $template_info['default_content'] as $section => $content ) {
						if ( $section == 'braintrust_avatars' ) {
							$avatars = brt_newsletter_get_avatars();
							echo "<h4>{$section}</h4><textarea name='html_{$section}' id='html_{$section}'>" . str_replace( "\t", '', str_replace( "   ", '', htmlspecialchars( $avatars ) ) ) . "</textarea>"; // Show avatars in text area
						}
						else {
							echo "<h4><span>{$section}</span></h4><div class='inside'><textarea name='html_{$section}' id='html_{$section}'>" . str_replace( "\t", '', str_replace( "   ", '', htmlspecialchars( $content ) ) ) . "</textarea></div>"; // Show text area. Remove all the extra tabs and spacing that usually shows up in these templates.
						}
					}
echo <<<EOF
					</form></div>
					</div>
EOF;

					echo <<<EOF

					<div style='clear:both;'>
						<input type="submit" class="button-primary" value="Next" id="step2_submit" />
					</div>
EOF;
				}
				break;

			case "step3":
				if ( get_option( 'mailchimp_api_key' ) == "" ) {
					echo "<p class='newsletter_error'>You must enter your MailChimp API key in the settings page before you can continue. " . $this->random['whoops'][rand( 0, count( $this->random['whoops'] ) - 1 )] . "</p>";
				}
				else {
					$api = new MCAPI_WordChimp( get_option( 'mailchimp_api_key' ) );

					$retval = $api->lists();

					if ( $api->errorCode ) {
						echo "<p class='newsletter_error'>Something went wrong when trying to get your MailChimp e-mail lists.  " . $this->random['whoops'][rand( 0, count( $this->random['whoops'] ) - 1 )] . " {$api->errorCode} {$api->errorMessage}</p>";
					}
					else {
						echo "<h1>Step 3: Select List</h1><span class='newsletter_notice'>Please select the MailChimp list you would like to send to.</span><br /><br />";
						echo <<<EOF
							<form method='post'>
								<input type='hidden' name='wp_cmd' value='step4' />
								<input type='hidden' name='template_id' value='{$_POST['template_id']}' />
EOF;

						// Add the previously sent posts
						foreach ( $_POST as $key => $value ) {
							if ( $key != 'wp_cmd' && $key != 'template_id' ) {
								echo "<input type='hidden' name='{$key}' value=\"" . htmlspecialchars( stripslashes( $value ) ) . "\" />";
							}
						}

						// Show MailChimp lists for selection
						foreach ( $retval['data'] as $count => $list ) {
							if ( $count == 0 ) {
								echo "<label><input type='radio' name='mailchimp_list_id' value='{$list['id']}' CHECKED /> {$list['name']} ({$list['stats']['member_count']} Subscribers)</label><br />";
							}
							else {
								echo "<label><input type='radio' name='mailchimp_list_id' value='{$list['id']}' /> {$list['name']} ({$list['stats']['member_count']} Subscribers)</label><br />";
							}
						}
						echo <<<EOF
						<p class="submit">
							<input type="submit" class="button-primary" value="Next" />
						</p>
					</form>
EOF;
					}
				}
				break;

			case "step4":
				if ( get_option( 'mailchimp_api_key' ) == "" ) {
					echo "<p class='newsletter_error'>You must enter your MailChimp API key in the settings page before you can continue. " . $this->random['whoops'][rand( 0, count( $this->random['whoops'] ) - 1 )] . "</p>";
				}
				else {
					echo <<<EOF
						<h1>Step 4: Complete Campaign Information</h1>
						<form method='post'>
							<input type='hidden' name='wp_cmd' value='step5' />
							<input type='hidden' name='template_id' value='{$_POST['template_id']}' />
							<input type='hidden' name='mailchimp_list_id' value='{$_POST['mailchimp_list_id']}' />
EOF;

					// Add modified template sections
					foreach ( $_POST as $key => $value ) {
						if ( $key != 'wp_cmd' && $key != 'template_id' && $key != 'mailchimp_list_id' ) {
							echo "<input type='hidden' name='{$key}' value=\"" . htmlspecialchars( stripslashes( $value ) ) . "\" />";
						}
					}

					echo "<span class='newsletter_notice'>Please enter the campaign information.</span><br /><br />";

					// Setup campaign options
					$newsletter_campaign_from_email = htmlspecialchars( get_option( 'newsletter_campaign_from_email' ) );
					$newsletter_campaign_from_name  = htmlspecialchars( get_option( 'newsletter_campaign_from_name' ) );
					echo <<<EOF
					<table class='newsletter_form_table'>
						<tr valign='top'>
							<th scope='row'>Title</th>
							<td><input type='text' name='newsletter_campaign_title' /></td>
						</tr>
						<tr valign='top'>
							<th scope='row'>Subject</th>
							<td><input type='text' name='newsletter_campaign_subject' /></td>
						</tr>
						<tr valign='top'>
							<th scope='row'>From Email</th>
							<td><input type='text' name='newsletter_campaign_from_email' value='{$newsletter_campaign_from_email}' /></td>
						</tr>
						<tr valign='top'>
							<th scope='row'>From Name</th>
							<td><input type='text' name='newsletter_campaign_from_name' value='{$newsletter_campaign_from_name}' /></td>
						</tr>
					</table>
					<h3>Campaign Tracking</h3>
					<label><input type='checkbox' name='newsletter_campaign_track_opens' value='true' checked /> Track number of times email was opened</label><br />
					<label><input type='checkbox' name='newsletter_campaign_track_html_clicks' value='true' checked /> Track number of times a user clicked an HTML link</label><br />
					<label><input type='checkbox' name='newsletter_campaign_track_text_clicks' value='true' checked /> Track number of times a user clicked a text link</label>
EOF;

					echo <<<EOF
						<p class="submit">
							<input type="submit" class="button-primary" value="Next" />
						</p>
					</form>
EOF;
				}
				break;

			case "step5":
				if ( get_option( 'mailchimp_api_key' ) == "" ) {
					echo "<p class='newsletter_error'>You must enter your MailChimp API key in the settings page before you can continue. " . $this->random['whoops'][rand( 0, count( $this->random['whoops'] ) - 1 )] . "</p>";
				}
				else {
					// Build the campaign
					$api = new MCAPI_WordChimp( get_option( 'mailchimp_api_key' ) );

					$type = 'regular';

					$opts['template_id']  = $_POST['template_id'];
					$opts['list_id']      = $_POST['mailchimp_list_id'];
					$opts['subject']      = $_POST['newsletter_campaign_subject'];
					$opts['from_email']   = $_POST['newsletter_campaign_from_email'];
					$opts['from_name']    = $_POST['newsletter_campaign_from_name'];
					$opts['tracking']     = array( 'opens' => $_POST['newsletter_campaign_track_opens'] == 'true' ? true : false, 'html_clicks' => $_POST['newsletter_campaign_track_html_clicks'] == 'true' ? true : false, 'text_clicks' => $_POST['newsletter_campaign_track_text_clicks'] == 'true' ? true : false );
					$opts['authenticate'] = true;
					$opts['title']        = $_POST['newsletter_campaign_title'];

					foreach ( $_POST as $key => $value ) {
						if ( $key != 'wp_cmd' && $key != 'template_id' && $key != 'mailchimp_list_id' ) {
							$content[$key] = stripslashes( $value );
						}
					}

					$campaignId = $api->campaignCreate( $type, $opts, $content );

					if ( $api->errorCode ) {
						echo "<p class='newsletter_error'>There was an error creating your campaign. " . $this->random['whoops'][rand( 0, count( $this->random['whoops'] ) - 1 )] . " {$api->errorCode} {$api->errorMessage}</p>";
					}
					else {
						echo "<h1>Step 5: Test/Send</h1><p class='newsletter_success'>Great success! Your campaign was created. {$this->random['compliment'][rand(0, count($this->random['compliment'])-1)]} Now what?</p>";

						echo "<a href='" . get_bloginfo( 'wpurl' ) . "/wp-admin/admin-ajax.php?action=newsletter_campaign_preview&cid={$campaignId}' target='_blank'>Preview Campaign in Browser</a><br /><br />";
						echo <<<EOF
						<h3>Send a test?</h3>
						<form method='post'>
							<input type='hidden' name='wp_cmd' value='step6' />
							<input type='hidden' name='template_id' value='{$_POST['template_id']}' />
							<input type='hidden' name='mailchimp_campaign_id' value='{$campaignId}' />
							<input type='text' name='mailchimp_test_emails' value='test@email.com, test@otheremail.com' onClick="this.value='';" />
EOF;
						// Add modified template sections
						foreach ( $_POST as $key => $value ) {
							if ( $key != 'wp_cmd' && $key != 'template_id' && $key != 'mailchimp_list_id' ) {
								echo "<input type='hidden' name='{$key}' value=\"" . htmlspecialchars( stripslashes( $value ) ) . "\" />";
							}
						}

						echo <<<EOF
							<p class="submit">
								<input type="submit" class="button-primary" value="Send Test" />
							</p>
						</form>
						<h3>Send fo' real?</h3>
						<form method='post'>
							<input type='hidden' name='wp_cmd' value='step7' />
							<input type='hidden' name='mailchimp_campaign_id' value='{$campaignId}' />
							<p class="submit">
								<input type="submit" class="button-primary" value="Send Fo' Real!" />
							</p>
						</form>
EOF;
					}
				}
				break;

			case "step6":
				$api        = new MCAPI_WordChimp( get_option( 'mailchimp_api_key' ) );
				$emails     = explode( ",", $_POST['mailchimp_test_emails'] );
				$campaignId = $_POST['mailchimp_campaign_id'];

				$retval = $api->campaignSendTest( $campaignId, $emails );

				if ( $api->errorCode ) {
					echo "<p class='newsletter_error'>Unable to send test campaign.  " . $this->random['whoops'][rand( 0, count( $this->random['whoops'] ) - 1 )] . " {$api->errorCode} {$api->errorMessage}</p>";
				}
				else {
					echo "<h1>Step 5: Test/Send</h1><span class='newsletter_notice'>Great success! A test has been sent to the e-mail addresses you provided. " . $this->random['compliment'][rand( 0, count( $this->random['compliment'] ) - 1 )] . "</span><br /><br />";
				}

				echo "<a href='" . get_bloginfo( 'wpurl' ) . "/wp-admin/admin-ajax.php?action=newsletter_campaign_preview&cid={$campaignId}' target='_blank'>Preview Campaign in Browser</a><br /><br />";
				echo <<<EOF
				<h3>Send a test?</h3>
				<form method='post'>
					<input type='hidden' name='wp_cmd' value='step6' />
					<input type='hidden' name='mailchimp_campaign_id' value='{$campaignId}' />
					<input type='text' name='mailchimp_test_emails' value='test@email.com, test@otheremail.com' onClick="this.value='';" />
EOF;
				// Add modified template sections
				foreach ( $_POST as $key => $value ) {
					if ( $key != 'wp_cmd' && $key != 'template_id' && $key != 'mailchimp_list_id' ) {
						echo "<input type='hidden' name='{$key}' value=\"" . htmlspecialchars( stripslashes( $value ) ) . "\" />";
					}
				}

				echo <<<EOF
					<p class="submit">
						<input type="submit" class="button-primary" value="Send Test" />
					</p>
				</form>
				<h3>Send fo' real?</h3>
				<form method='post'>
					<input type='hidden' name='wp_cmd' value='step7' />
					<input type='hidden' name='mailchimp_campaign_id' value='{$campaignId}' />
					<p class="submit">
						<input type="submit" class="button-primary" value="Send Fo' Real!" />
					</p>
				</form>
EOF;
				break;

			case "step7":
				$api        = new MCAPI_WordChimp( get_option( 'mailchimp_api_key' ) );
				$campaignId = $_POST['mailchimp_campaign_id'];
				$emails     = explode( ",", $_POST['mailchimp_test_emails'] );

				$retval = $api->campaignSendNow( $campaignId );

				if ( $api->errorCode ) {
					echo "<p class='newsletter_error'>Unable to send out campaign.  " . $this->random['whoops'][rand( 0, count( $this->random['whoops'] ) - 1 )] . " {$api->errorCode} {$api->errorMessage}</p>";
				}
				else {
					echo "<h1>Ding! Campaign Sent</h1><p class='newsletter_success'>Great success! You're campaign has been sent out. " . $this->random['compliment'][rand( 0, count( $this->random['compliment'] ) - 1 )] . "</p><br /><br />";
				}
				break;
		}

		echo <<<EOF
		</div>
EOF;
	}


	function sbnewsletter_campaigns_page() {
		global $wpdb;

		echo <<<EOF
		<div class='wrap sbnewsletter'>
			<div id='icon-themes' class='icon32'></div>
			<h2 class="nav-tab-wrapper">
				<a href="admin.php?page=sbnewsletter" class="nav-tab">Dashboard</a>
				<a href="admin.php?page=sbnewsletter-campaigns" class="nav-tab nav-tab-active">Stats</a>
				<a href="options-general.php?page=sbnewsletter/settings" class="nav-tab">Settings</a>
			</h2>
EOF;

		echo "<h1>Campaigns</h1>";
		echo "<p class='newsletter_notice'>Shows information and statistics for all sent campaigns (generated through the site or otherwise)</p>";
		if ( get_option( 'mailchimp_api_key' ) == "" ) {
			echo "<p class='newsletter_error'>You must enter your MailChimp API key in the settings page before you can continue. " . $this->random['whoops'][rand( 0, count( $this->random['whoops'] ) - 1 )] . "</p>";
		}
		else {
			$api       = new MCAPI_WordChimp( get_option( 'mailchimp_api_key' ) );
			$campaigns = $api->campaigns();

			if ( $api->errorCode ) {
				echo "<p class='newsletter_error'>Sorry, we were unable to get a list of your campaigns. " . $this->random['whoops'][rand( 0, count( $this->random['whoops'] ) - 1 )] . " {$api->errorCode} {$api->errorMessage}</p>";
			}
			elseif ( sizeof( $campaigns['data'] ) <= 0 ) {
				echo "<p class='newsletter_error'>Sorry, there are no campaigns available that have been sent. Please send a campaign and come on back to take stats for a spin!</p>";
			}
			else {
				echo <<<EOF
					<table class='newsletter_campaigns_analytics_table'>
						<tbody>
EOF;
				foreach ( $campaigns['data'] as $campaign ) {
					if ( $campaign['status'] == 'sent' ) {
						$stats = $api->campaignStats( $campaign['id'] );

						if ( $api->errorCode ) {
							echo "<p class='newsletter_error'>Sorry, we were unable to get analytics for a campaign. " . $this->random['whoops'][rand( 0, count( $this->random['whoops'] ) - 1 )] . " {$api->errorCode} {$api->errorMessage}</p>";
						}
						else {
							$newsletter_timestamp_format = get_option( 'newsletter_timestamp_format' ) == '' ? 'm/d/Y g:ia' : get_option( 'newsletter_timestamp_format' );

							$campaign['send_time'] = date( $newsletter_timestamp_format, strtotime( $campaign['send_time'] ) );
							$stats['last_open']    = $stats['last_open'] != '' ? date( $newsletter_timestamp_format, strtotime( $stats['last_open'] ) ) : 'NEVA';
							$stats['last_click']   = $stats['last_click'] != '' ? date( $newsletter_timestamp_format, strtotime( $stats['last_click'] ) ) : 'NEVA';

							echo <<<EOF
								<tr class='newsletter_campaign_separate'>
									<td colspan='6'></td>
								</tr>
								<tr>
									<td colspan='6' class='newsletter_campaign_title'><a href='" . get_bloginfo( 'wpurl' ) . "/wp-admin/admin-ajax.php?action=newsletter_campaign_preview&cid={$campaign['id']}' target='_blank'>{$campaign['title']}</a></td>
								</tr>
								<tr>
									<td><span class='newsletter_campaign_data_title'>Sent on:</span><br /><span class='newsletter_campaign_data_value'>{$campaign['send_time']}</span></td>
									<td><span class='newsletter_campaign_data_title'>Emails sent:</span><br /><span class='newsletter_campaign_data_value'>{$campaign['emails_sent']}</span></td>
									<td><span class='newsletter_campaign_data_title'>Hard bounces:</span><br /><span class='newsletter_campaign_data_value'>{$stats['hard_bounces']}</span></td>
									<td><span class='newsletter_campaign_data_title'>Soft bounces:</span><br /><span class='newsletter_campaign_data_value'>{$stats['soft_bounces']}</span></td>
									<td><span class='newsletter_campaign_data_title'>Unsubscribes:</span><br /><span class='newsletter_campaign_data_value'>{$stats['unsubscribes']}</span></td>
									<td><span class='newsletter_campaign_data_title'>Forwards:</span><br /><span class='newsletter_campaign_data_value'>{$stats['forwards']}</span></td>
								</tr>
								<tr>
									<td><span class='newsletter_campaign_data_title'>Forwards opens:</span><br /><span class='newsletter_campaign_data_value'>{$stats['forwards_opens']}</span></td>
									<td><span class='newsletter_campaign_data_title'>Unique Opens:</span><br /><span class='newsletter_campaign_data_value'>{$stats['unique_opens']}</span></td>
									<td><span class='newsletter_campaign_data_title'>Last open:</span><br /><span class='newsletter_campaign_data_value'>{$stats['last_open']}</span></td>
									<td><span class='newsletter_campaign_data_title'>Clicks:</span><br /><span class='newsletter_campaign_data_value'>{$stats['clicks']}</span></td>
									<td><span class='newsletter_campaign_data_title'>Users who clicked:</span><br /><span class='newsletter_campaign_data_value'>{$stats['users_who_clicked']}</span></td>
									<td><span class='newsletter_campaign_data_title'>Last click:</span><br /><span class='newsletter_campaign_data_value'>{$stats['last_click']}</span></td>
								</tr>
EOF;
						}
					}
				}
				echo "</tbody></table>";
			}
		}
		echo "</div>";
	}

	function sbnewsletter_settings_page() {

		echo <<<EOF
		<div class='wrap sbnewsletter'>
			<div id='icon-themes' class='icon32'></div>
			<h2 class="nav-tab-wrapper">
				<a href="admin.php?page=sbnewsletter" class="nav-tab">Dashboard</a>
				<a href="admin.php?page=sbnewsletter-campaigns" class="nav-tab">Stats</a>
				<a href="options-general.php?page=sbnewsletter/settings" class="nav-tab nav-tab-active">Settings</a>
			</h2>
EOF;
		echo "<form method='post' action='options.php'> ";
		settings_fields( 'sbnewsletter_options_group' );
		do_settings_fields( 'newsletter_settings_page', 'sbnewsletter_options_group' );

		$mailchimp_api_key             = get_option( 'mailchimp_api_key' );
		$newsletter_campaign_from_name  = get_option( 'newsletter_campaign_from_name' );
		$newsletter_campaign_from_email = get_option( 'newsletter_campaign_from_email' );
	//	$newsletter_strip_images_checked = get_option( 'newsletter_strip_images' ) == true ? 'CHECKED' : '';
	//	$newsletter_show_author_checked = get_option( 'newsletter_show_author' ) == true ? 'CHECKED' : '';
	//	$newsletter_show_timestamp_checked = get_option( 'newsletter_show_timestamp' ) == true ? 'CHECKED' : '';
		$newsletter_timestamp_format = get_option( 'newsletter_timestamp_format' ) == '' ? 'm/d/Y g:ia' : get_option( 'newsletter_timestamp_format' );
	//	$newsletter_use_excerpt_checked = get_option( 'newsletter_use_excerpt' ) == true ? 'CHECKED' : '';
		$newsletter_page_capability      = get_option( 'newsletter_page_capability' ) == '' ? 'manage_options' : get_option( 'newsletter_page_capability' );
		$newsletter_campaigns_capability = get_option( 'newsletter_campaigns_capability' ) == '' ? 'manage_options' : get_option( 'newsletter_campaigns_capability' );
		$newsletter_settings_capability  = get_option( 'newsletter_settings_capability' ) == '' ? 'manage_options' : get_option( 'newsletter_settings_capability' );

		if ( isset( $_GET['settings-updated'] ) && $_GET['settings-updated'] == 'true' ) {
			echo "<p class='newsletter_success'>Great success! Your settings have been updated. " . $this->random['compliment'][rand( 0, count( $this->random['compliment'] ) - 1 )] . "</p>";
		}

		echo <<<EOF
			<h1>Settings</h1>
			<h3>APIs</h3>
			<label><strong>MailChimp API Key</strong><br /><input type='text' name='mailchimp_api_key' value='{$mailchimp_api_key}' /></label><br/><br/>
			<h3>Security</h3>
			<small>Security is based on WordPress 'capabilities'. The 'manage_option' capability is the default and if you don't know what this is or how this works, please do not change it! For more information, visit <a href='http://codex.wordpress.org/Roles_and_Capabilities' target='_blank'>http://codex.wordpress.org/Roles_and_Capabilities</a>.</small><br />
			<label><strong>Create Campaign Through the site:</strong><br />
				<select name='newsletter_page_capability'>
					<option>{$newsletter_page_capability}</option>
					<option disabled>-----</option>
					<option disabled value=''>Super Admin</option>
					<option>manage_network</option>
					<option>manage_sites</option>
					<option>manage_network_users</option>
					<option>manage_network_themes</option>
					<option>manage_network_options</option>
					<option>unfiltered_html when using Multisite</option>
					<option disabled value=''>Administrator</option>
					<option>activate_plugins</option>
					<option>add_users</option>
					<option>create_users</option>
					<option>delete_others_pages</option>
					<option>delete_others_posts</option>
					<option>delete_pages</option>
					<option>delete_plugins</option>
					<option>delete_posts</option>
					<option>delete_private_pages</option>
					<option>delete_private_posts</option>
					<option>delete_published_pages</option>
					<option>delete_published_posts</option>
					<option>delete_themes</option>
					<option>delete_users</option>
					<option>edit_dashboard</option>
					<option>edit_files</option>
					<option>edit_others_pages</option>
					<option>edit_others_posts</option>
					<option>edit_pages</option>
					<option>edit_plugins</option>
					<option>edit_posts</option>
					<option>edit_private_pages</option>
					<option>edit_private_posts</option>
					<option>edit_published_pages</option>
					<option>edit_published_posts</option>
					<option>edit_theme_options</option>
					<option>edit_themes</option>
					<option>edit_users</option>
					<option>export</option>
					<option>import</option>
					<option>install_plugins</option>
					<option>install_themes</option>
					<option>list_users</option>
					<option>manage_categories</option>
					<option>manage_links</option>
					<option>manage_options</option>
					<option>moderate_comments</option>
					<option>promote_users</option>
					<option>publish_pages</option>
					<option>publish_posts</option>
					<option>read_private_pages</option>
					<option>read_private_posts</option>
					<option>read</option>
					<option>remove_users</option>
					<option>switch_themes</option>
					<option>unfiltered_html</option>
					<option>unfiltered_upload</option>
					<option>update_core</option>
					<option>update_plugins</option>
					<option>update_themes</option>
					<option>upload_files</option>
					<option disabled value=''>Editor</option>
					<option>delete_others_pages</option>
					<option>delete_others_posts</option>
					<option>delete_pages</option>
					<option>delete_posts</option>
					<option>delete_private_pages</option>
					<option>delete_private_posts</option>
					<option>delete_published_pages</option>
					<option>delete_published_posts</option>
					<option>edit_others_pages</option>
					<option>edit_others_posts</option>
					<option>edit_pages</option>
					<option>edit_posts</option>
					<option>edit_private_pages</option>
					<option>edit_private_posts</option>
					<option>edit_published_pages</option>
					<option>edit_published_posts</option>
					<option>manage_categories</option>
					<option>manage_links</option>
					<option>moderate_comments</option>
					<option>publish_pages</option>
					<option>publish_posts</option>
					<option>read</option>
					<option>read_private_pages</option>
					<option>read_private_posts</option>
					<option>unfiltered_html</option>
					<option>upload_files</option>
					<option disabled value=''>Author</option>
					<option>delete_posts</option>
					<option>delete_published_posts</option>
					<option>edit_posts</option>
					<option>edit_published_posts</option>
					<option>publish_posts</option>
					<option>read</option>
					<option>upload_files</option>
					<option disabled value=''>Contributor</option>
					<option>delete_posts</option>
					<option>edit_posts</option>
					<option>read</option>
					<option disabled value=''>Subscriber</option>
					<option>read</option>
				</select>
			</label><br /><br />
			<label><strong>Access Campaign Stats Through the site:</strong><br />
				<select name='newsletter_campaigns_capability'>
					<option>{$newsletter_campaigns_capability}</option>
					<option disabled>-----</option>
					<option disabled value=''>Super Admin</option>
					<option>manage_network</option>
					<option>manage_sites</option>
					<option>manage_network_users</option>
					<option>manage_network_themes</option>
					<option>manage_network_options</option>
					<option>unfiltered_html when using Multisite</option>
					<option disabled value=''>Administrator</option>
					<option>activate_plugins</option>
					<option>add_users</option>
					<option>create_users</option>
					<option>delete_others_pages</option>
					<option>delete_others_posts</option>
					<option>delete_pages</option>
					<option>delete_plugins</option>
					<option>delete_posts</option>
					<option>delete_private_pages</option>
					<option>delete_private_posts</option>
					<option>delete_published_pages</option>
					<option>delete_published_posts</option>
					<option>delete_themes</option>
					<option>delete_users</option>
					<option>edit_dashboard</option>
					<option>edit_files</option>
					<option>edit_others_pages</option>
					<option>edit_others_posts</option>
					<option>edit_pages</option>
					<option>edit_plugins</option>
					<option>edit_posts</option>
					<option>edit_private_pages</option>
					<option>edit_private_posts</option>
					<option>edit_published_pages</option>
					<option>edit_published_posts</option>
					<option>edit_theme_options</option>
					<option>edit_themes</option>
					<option>edit_users</option>
					<option>export</option>
					<option>import</option>
					<option>install_plugins</option>
					<option>install_themes</option>
					<option>list_users</option>
					<option>manage_categories</option>
					<option>manage_links</option>
					<option>manage_options</option>
					<option>moderate_comments</option>
					<option>promote_users</option>
					<option>publish_pages</option>
					<option>publish_posts</option>
					<option>read_private_pages</option>
					<option>read_private_posts</option>
					<option>read</option>
					<option>remove_users</option>
					<option>switch_themes</option>
					<option>unfiltered_html</option>
					<option>unfiltered_upload</option>
					<option>update_core</option>
					<option>update_plugins</option>
					<option>update_themes</option>
					<option>upload_files</option>
					<option disabled value=''>Editor</option>
					<option>delete_others_pages</option>
					<option>delete_others_posts</option>
					<option>delete_pages</option>
					<option>delete_posts</option>
					<option>delete_private_pages</option>
					<option>delete_private_posts</option>
					<option>delete_published_pages</option>
					<option>delete_published_posts</option>
					<option>edit_others_pages</option>
					<option>edit_others_posts</option>
					<option>edit_pages</option>
					<option>edit_posts</option>
					<option>edit_private_pages</option>
					<option>edit_private_posts</option>
					<option>edit_published_pages</option>
					<option>edit_published_posts</option>
					<option>manage_categories</option>
					<option>manage_links</option>
					<option>moderate_comments</option>
					<option>publish_pages</option>
					<option>publish_posts</option>
					<option>read</option>
					<option>read_private_pages</option>
					<option>read_private_posts</option>
					<option>unfiltered_html</option>
					<option>upload_files</option>
					<option disabled value=''>Author</option>
					<option>delete_posts</option>
					<option>delete_published_posts</option>
					<option>edit_posts</option>
					<option>edit_published_posts</option>
					<option>publish_posts</option>
					<option>read</option>
					<option>upload_files</option>
					<option disabled value=''>Contributor</option>
					<option>delete_posts</option>
					<option>edit_posts</option>
					<option>read</option>
					<option disabled value=''>Subscriber</option>
					<option>read</option>
				</select>
			</label><br /><br />
			<label><strong>Access to Newsletter Settings:</strong><br />
				<select name='newsletter_settings_capability'>
					<option>{$newsletter_settings_capability}</option>
					<option disabled>-----</option>
					<option disabled value=''>Super Admin</option>
					<option>manage_network</option>
					<option>manage_sites</option>
					<option>manage_network_users</option>
					<option>manage_network_themes</option>
					<option>manage_network_options</option>
					<option>unfiltered_html when using Multisite</option>
					<option disabled value=''>Administrator</option>
					<option>activate_plugins</option>
					<option>add_users</option>
					<option>create_users</option>
					<option>delete_others_pages</option>
					<option>delete_others_posts</option>
					<option>delete_pages</option>
					<option>delete_plugins</option>
					<option>delete_posts</option>
					<option>delete_private_pages</option>
					<option>delete_private_posts</option>
					<option>delete_published_pages</option>
					<option>delete_published_posts</option>
					<option>delete_themes</option>
					<option>delete_users</option>
					<option>edit_dashboard</option>
					<option>edit_files</option>
					<option>edit_others_pages</option>
					<option>edit_others_posts</option>
					<option>edit_pages</option>
					<option>edit_plugins</option>
					<option>edit_posts</option>
					<option>edit_private_pages</option>
					<option>edit_private_posts</option>
					<option>edit_published_pages</option>
					<option>edit_published_posts</option>
					<option>edit_theme_options</option>
					<option>edit_themes</option>
					<option>edit_users</option>
					<option>export</option>
					<option>import</option>
					<option>install_plugins</option>
					<option>install_themes</option>
					<option>list_users</option>
					<option>manage_categories</option>
					<option>manage_links</option>
					<option>manage_options</option>
					<option>moderate_comments</option>
					<option>promote_users</option>
					<option>publish_pages</option>
					<option>publish_posts</option>
					<option>read_private_pages</option>
					<option>read_private_posts</option>
					<option>read</option>
					<option>remove_users</option>
					<option>switch_themes</option>
					<option>unfiltered_html</option>
					<option>unfiltered_upload</option>
					<option>update_core</option>
					<option>update_plugins</option>
					<option>update_themes</option>
					<option>upload_files</option>
					<option disabled value=''>Editor</option>
					<option>delete_others_pages</option>
					<option>delete_others_posts</option>
					<option>delete_pages</option>
					<option>delete_posts</option>
					<option>delete_private_pages</option>
					<option>delete_private_posts</option>
					<option>delete_published_pages</option>
					<option>delete_published_posts</option>
					<option>edit_others_pages</option>
					<option>edit_others_posts</option>
					<option>edit_pages</option>
					<option>edit_posts</option>
					<option>edit_private_pages</option>
					<option>edit_private_posts</option>
					<option>edit_published_pages</option>
					<option>edit_published_posts</option>
					<option>manage_categories</option>
					<option>manage_links</option>
					<option>moderate_comments</option>
					<option>publish_pages</option>
					<option>publish_posts</option>
					<option>read</option>
					<option>read_private_pages</option>
					<option>read_private_posts</option>
					<option>unfiltered_html</option>
					<option>upload_files</option>
					<option disabled value=''>Author</option>
					<option>delete_posts</option>
					<option>delete_published_posts</option>
					<option>edit_posts</option>
					<option>edit_published_posts</option>
					<option>publish_posts</option>
					<option>read</option>
					<option>upload_files</option>
					<option disabled value=''>Contributor</option>
					<option>delete_posts</option>
					<option>edit_posts</option>
					<option>read</option>
					<option disabled value=''>Subscriber</option>
					<option>read</option>
				</select>
			</label><br /><br />
			<h3>Options</h3>
			<label><strong>Default From Name:</strong><br /><input type='text' name='newsletter_campaign_from_name' value='{$newsletter_campaign_from_name}' /></label><br /><br />
			<label><strong>Default From E-mail:</strong><br /><input type='text' name='newsletter_campaign_from_email' value='{$newsletter_campaign_from_email}' /></label><br /><br />
			<label><strong>Date/Time Format:</strong><br /><input type='text' name='newsletter_timestamp_format' value='{$newsletter_timestamp_format}' /></label><br /><br />
			<p class="submit">
				<input type="submit" class="button-primary" value="Save Changes" />
			</p>
		</form>
EOF;

		echo "</div>";
	}

	function newsletter_campaign_preview() {
		global $wpdb;

		$api = new MCAPI_WordChimp( get_option( 'mailchimp_api_key' ) );

		$campaignContent = $api->campaignContent( $_GET['cid'] );
		$type            = ( isset( $_GET['type'] ) ) ? $_GET['type'] : "";
		switch ( $type ) {
			default:
			case "html":
				echo $campaignContent['html'];
				break;

			case "text":
				echo "<html><body><pre>{$campaignContent['text']}</pre></body></html>";
				break;
		}
		die();
	}


}

new SB_Newsletter();


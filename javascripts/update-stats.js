jQuery(document).ready(function() {
	jQuery('.update_stats').on('click', function(event){
		event.preventDefault();
		var tweet_id = jQuery(this).attr('id');
		jQuery('.stats_'+tweet_id).html('<img src="'+loading_img+'" />');
//		console.log('this id: '+tweet_id);
			jQuery.ajax({
				url: ajaxurl,
				type: "POST",
				dataType: 'json',
				data: {tweet: tweet_id, format: 'json', action: 'brt_update_tweet_stats'}
			}).done(function (data) {
				jQuery('.stats_'+tweet_id).html(data.stats);
				jQuery('.shared_'+tweet_id).html(data.shared);
				jQuery('.globalclicks_'+tweet_id).html(data.globalclicks);
				jQuery('.localclicks_'+tweet_id).html(data.localclicks);
				jQuery('.retweets_'+tweet_id).html(data.retweets);
				});
	});

	jQuery('#sshare_remove_thumb_btn').on('click', function(){
		jQuery('#sshare_thumb').val('');
		jQuery('#sshare_thumb_img').attr('src', '');
	});
	jQuery('.xhour').on('click', function(event) {
    event.preventDefault();
		jQuery(this).colorbox(
			{
				inline:true,
				width:"80%",
        height:"451px",
				onComplete:function(){
          jQuery('#graph').html('');
					var tweet_id = jQuery(this).attr('id');
					var time = jQuery(this).data('hour');
					jQuery.ajax({
						url: ajaxurl,
						type: "POST",
						dataType: 'json',
						data: {tweet: tweet_id, time: time, format: 'json', action: 'brt_xhour'}
					}).done(function (data) {

/*var opts = {
  "dataFormatX": function (x) { return d3.time.format('%Y-%m-%d %X').parse(x); },
  "tickFormatX": function (x) { return d3.time.format('%H %p')(x); }
};
var myChart = new xChart('line-dotted', data, '#myChart', opts);*/
//						jQuery('.stats_'+tweet_id).html(data);
  g = new Dygraph(

    // containing div
    document.getElementById("graph"),

    // CSV or path to a CSV file.
    "Date/time,Clicks,Global Clicks,Shares,Retweets\n" +data
  );
					});
				}
			}
		);
	});
});

